    <p align="justify">JUNE 18. </p>
    <p align="justify">&quot;But the anointing which you have received of him abides 
    in you, and you need not that any man teach you: but as the same anointing 
    takes you of all things, and is truth, and is no lie, and even as it has 
    taught you, you shall abide in him.&quot; 1 John 2:27 </p>
    <p align="justify">&quot;The Lord's anointed&quot; is the expressive and appropriate 
    designation of all the Lord's people. This anointing it is that marks them 
    as a &quot;chosen generation, a royal priesthood, a holy nation, a peculiar 
    people.&quot; It is the Lord's peculiar mark upon those who distinguishes and 
    designates them as His own. All who are strangers to this anointing are 
    strangers to the grace of God and the calling of the Holy Spirit. There may 
    be much spiritual light in the judgment, and even an open profession of 
    religion before the world, added to which there shall be something of Jehu's 
    &quot;zeal for the Lord;&quot; and yet that anointing of the Holy Spirit be still 
    lacking, apart from which all intellectual illumination, and outward 
    profession, and party zeal, pass for nothing with a heart searching God. As 
    the proper signification of the endeared name, Christ, is anointed, so the 
    true signification of the honored appellation, Christian, points us to the 
    anointing, of which all who have union with Christ personally share. I 
    believe the remark to be as solemn as it is true, that eternity will only 
    fully unfold the amount of evil that has sprung from calling those 
    Christians who call themselves Christians, without any valid title to the 
    high, holy, and distinguished appellation. How imperfectly are men in 
    general aware of the deep, the significant, the spiritual import of the 
    term! They think not, they know not, that a Christian is one who partakes, 
    in His renewing, sanctifying grace, of that same Divine Holy Spirit with 
    which Christ was anointed of the Father for His great work. <br>
    The effects of this anointing are what might be expected from a cause so 
    glorious. It beautifies the soul. It is that anointing spoken of by the 
    Psalmist: &quot;And oil to make his face to shine.&quot; Therefore it is called the 
    &quot;beauties of holiness.&quot; How does a man's face shine- how is his countenance 
    lighted up- when the joy of the Lord is his strength, when the spirit of 
    adoption is in his soul, when the love of God is shed abroad in his heart! 
    It gladdens too. Therefore it is called the &quot;oil of joy&quot; and &quot;the oil of 
    gladness.&quot; It causes the heart to sing in its deep sorrows, imparts the 
    &quot;garment of praise for the spirit of heaviness,&quot; and fills the soul with the 
    glory of that &quot;kingdom which consists not in foods and in drinks, but in 
    righteousness and peace and joy in the Holy Spirit.&quot; Another effect 
    springing from this anointing is the deep teaching it imparts- &quot;You have an 
    anointing from the Holy One, and you know all things.&quot; Such are some of the 
    effects of this holy anointing. It beautifies, gladdens, and teaches. <br>
&nbsp; </p>
