    SEPTEMBER 13.<br>
    <br>
    &quot;Behold what manner of love the Father has bestowed upon us, that we should 
    be called the sons of God: therefore the world knows us not, because it knew 
    him not.&quot; 1 John 3:1<br>
    <br>
    IT is not strange that the fact of his adoption should meet with much 
    misgiving in the Christian's mind, seeing that it is a truth so spiritual, 
    flows from a source so concealed, and has its seat in the profound recesses 
    of the soul. The very stupendousness of the relationship staggers our 
    belief. To be fully assured of our divine adoption demands other than the 
    testimony either of our own feelings, or the opinion of men. Our 
    feelings-sometimes excited and visionary-may mislead; the opinion of 
    others-often fond and partial-may deceive us. The grand, the divine, and 
    only safe testimony is &quot;the Spirit itself bears witness with our spirit.&quot; 
    There exists a strong combination of evil, tending to shake the Christian's 
    confidence in the belief of his sonship. Satan is ever on the watch to 
    insinuate the doubt. He tried the experiment with our Lord: &quot;If You be the 
    Son of God.&quot; In no instance would it appear that he actually denied the 
    truth of Christ's Divine Sonship; the utmost that his temerity permitted was 
    the suggestion to the mind of a doubt; leaving it there to its own working. 
    Our blessed Lord thus assailed, it is no marvel that His disciples should be 
    exposed to a like assault. The world, too, presumes to call it in question. 
    &quot;The world knows us not, because it knew Him not.&quot; Ignorant of the Divine 
    Original, how can it recognize the Divine lineaments in the faint and 
    imperfect copy? It has no vocabulary by which it can decipher the &quot;new name 
    written in the white stone.&quot; The sons of God are in the midst of a crooked 
    and perverse nation, illumining it with their light, and preserving it by 
    their grace, yet disguised from its knowledge, and hidden from its view. But 
    the strongest doubts touching the validity of his adoption are those 
    engender in the believer's own mind. Oh! there is much there to generate and 
    foster the painful misgiving. We have said that the very greatness of the 
    favor, the stupendousness of the relationship, startles the mind, and 
    staggers our faith. &quot;What! to be a child of God! God my Father! can I be the 
    subject of a change so great, of a relationship so exalted? Who am I, O Lord 
    God, and what is my house, that You should exalt me to be a King's son? Is 
    this the manner of men, O Lord God?&quot; And then, there crowd upon the 
    believer's mind thoughts of his own sinfulness and unworthiness of so 
    distinguished a blessing. &quot;Can it be? with such a depravity of heart, such 
    carnality of mind, such rebellion of will, such a propensity to evil each 
    moment, and in everything such backslidings and flaws, does there yet exist 
    within me a nature that links me with the Divine? It seems impossible!&quot; And 
    when to all this are added the varied dispensations of his Heavenly Father, 
    often wearing a rough garb, assuming an aspect somber, threatening, and 
    crushing, oh, it is no marvel that, staggered by a discipline so severe, the 
    fact of God's love to him, and of his close and tender relation to God, 
    should sometimes be a matter of painful doubt; that thus he should 
    reason-&quot;If His child, reposing in His heart, and sealed upon His arm, why is 
    it thus? Would He not have spared me this heavy stroke? Would not this cup 
    have passed my lips? Would He have asked me to slay my Isaac, to resign my 
    Benjamin? All these things are against me.&quot; And thus are the children of God 
    constantly tempted to question the fact of their adoption.<br>
    <br>
    <br>
