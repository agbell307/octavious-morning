    NOVEMBER 28.<br>
    <br>
    &quot;As many as were ordained to eternal life believed.&quot; Acts 13:48<br>
    <br>
    THERE can be nothing in the Bible adverse to the salvation of a sinner. The 
    doctrine of predestination is a revealed doctrine of the Bible; therefore 
    predestination cannot be opposed to the salvation of the sinner. So far from 
    this being true, we hesitate not most strongly and emphatically to affirm, 
    that we know of no doctrine of God&rsquo;s word more replete with encouragement to 
    the awakened, sin-burdened, Christ-seeking soul than this. What stronger 
    evidence can we have of our election of God than the Spirit&rsquo;s work in the 
    heart? Are you really in earnest for the salvation of your soul? Do you feel 
    the plague of sin? Are you sensible of the condemnation of the law? Do you 
    come under the denomination of the &quot;weary and heavy laden&quot;? If so, then the 
    fact that you are a subject of the Divine drawings--that you have a felt 
    conviction of your sinfulness--and that you are looking wistfully for a place 
    of refuge, affords the strongest ground for believing that you are one of 
    those whom God has predestinated to eternal life. The very work thus begun 
    is the Spirit&rsquo;s first outline of the Divine image upon your soul--that very 
    image to which the saints are predestinated to be conformed. <br>
    <br>
    But while we thus vindicate this doctrine from being inimical to the 
    salvation of the anxious soul, we must with all distinctness and earnestness 
    declare, that in this stage of your Christian course you have primarily and 
    mainly to do with another and a different doctrine. We refer to the doctrine 
    of the Atonement. Could you look into the book of the Divine decrees, and 
    read your name inscribed upon its pages, it would not impart the joy and 
    peace which one believing view of Christ crucified will convey. It is not 
    essential to your salvation that you believe in election; but it is 
    essential to your salvation that you believe in the Lord Jesus Christ. In 
    your case, as an individual debating the momentous question how a sinner may 
    be justified before God, your first business is with Christ, and Christ 
    exclusively. You are to feel that you are a lost sinner, not that you are an 
    elect saint. The doctrine which meets the present phase of your spiritual 
    condition is, not the doctrine of predestination, but the doctrine of an 
    atoning Savior. The truth to which you are to give the first consideration 
    and the most simple and unquestioning credence is, that &quot;Christ died for the 
    ungodly&quot;--that He came into the world to save sinners--that He came to call, 
    not the righteous, but sinners to repentance--that in all respects, in the 
    great business of our salvation, He stands to us in the relation of a 
    Savior, while we stand before Him in the character of a sinner. Oh, let one 
    object fix your eye, and one theme fill your mind--Christ and His salvation. 
    Absorbed in the contemplation and study of these two points, you may safely 
    defer all further inquiry to another and a more advanced stage of your 
    Christian course. Remember that the fact of your predestination, the 
    certainty of your election, can only be inferred from your conversion. We 
    must hold you firmly to this truth. It is the subtle and fatal reasoning of 
    Satan, a species of atheistical fatalism, to argue, &quot;If I am elected I shall 
    be saved, whether I am regenerated or not.&quot; The path to eternal woe is paved 
    with arguments like this. Men have cajoled their souls with such vain 
    excuses until they have found themselves beyond the region of hope! But we 
    must rise to the fountain, by pursuing the stream. Conversion, and not 
    predestination, is the end of the chain we are to grasp. We must ascend from 
    ourselves to God, and not descend from God to ourselves, in settling this 
    great question. We must judge of God&rsquo;s objective purpose of love concerning 
    us, by His subjective work of grace within us. In conclusion, we earnestly 
    entreat you to lay aside all fruitless speculations, and to give yourself to 
    prayer. Let reason bow to faith, and faith shut you up to Christ, and Christ 
    be all in all to you. Beware that you come not short of true conversion--a 
    changed heart, and a renewed mind, so that you become a &quot;new creature in 
    Christ Jesus.&quot; And if as a poor lost sinner you repair to the Savior, all 
    vile and guilty, unworthy and weak as you are, He will receive you and 
    shelter you within the bosom that bled on the cross to provide an atonement 
    and an asylum for the very chief of sinners.<br>
    <br>
    <br>
