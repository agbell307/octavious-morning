    <p align="justify">MARCH 18. </p>
    <p align="justify">&quot;I John, who also am your brother, and companion in 
    tribulation, and in the kingdom and patience of Jesus Christ, was in the 
    isle that is called Patmos, for the word of God, and for the testimony of 
    Jesus Christ. I was in the Spirit on the Lord's day.&quot; Rev. 1:9-10. </p>
    <p align="justify">Our adorable Immanuel frequently reveals the most 
    brilliant beams of His glory in seasons of the most painful trial and 
    deepest gloom. The dark providential dispensations of God often bring out in 
    richer radiance the glories of His beloved Son, as the darkness of night 
    reveals more distinctly and brightly the existence and beauty of the 
    heavenly bodies. For the manifestation of this remarkable revelation of His 
    risen glory to His servant, our Lord selects precisely such an occasion- an 
    occasion which, to the eye of reason, would appear the most unfavorable and 
    improbable; but to faith's eye, ranging beyond second causes, the most 
    appropriate for such a revelation of Jesus. The emperor Domitian, though not 
    released from his fearful responsibility for the act, was but the instrument 
    of executing the eternal purpose of grace and love. God's hand was moving, 
    and moving too, as it often does, in the &quot;thick darkness.&quot; Exiled as John 
    was by this Roman emperor to a desolate island of the Aegean Sea, &quot;for the 
    word of God, and for the testimony of Jesus Christ,&quot; the Redeemer was but 
    preparing the way for the revelation of those visions of glory, than which, 
    none more sublime or more precious ever broke upon the eye of mortal man. 
    God was not only placing His beloved servant in a right posture to behold 
    them, but was also most wisely and graciously training and disciplining His 
    mind spiritually and humbly to receive them. <br>
    But mark how this dark and trying incident was making for the good of this 
    holy exile. Banished though he was from the saints, from society, and from 
    all means of grace, man could not banish him from the presence of God; nor 
    persecution separate him from the love of Christ. Patmos, to his view, 
    became resplendent with the glory of a risen Savior- a reconciled God and 
    Father was his Sanctuary- the Holy Spirit, the Comforter, overshadowed him- 
    and the Lord's day, already so hallowed and precious to him in its 
    association with the resurrection of the Lord, broke upon him with unwonted 
    effulgence, sanctity, and joy. Oh, how richly favored was this beloved 
    disciple! Great as had been his previous privileges- journeying with Christ, 
    beholding His miracles, hanging on His lips, reposing on His bosom- yet 
    never had he been so privileged- never had he learned so much of Jesus, nor 
    had seen so much of His glory, nor had drunk so deeply of His love, nor had 
    experienced so richly His unutterable tenderness, gentleness, and sympathy; 
    and never had he spent such a Lord's day as now, the solitary in habitant of 
    an isolated isle though he was. Oh, where is there a spot which Jesus cannot 
    irradiate with His glory; where is there solitude which He cannot sweeten 
    with His presence; where is there suffering, privation, and loss, which He 
    cannot more than recompense by His sustaining grace and soothing love; and 
    where is there a trembling and prostrate soul, which His &quot;right hand&quot; cannot 
    lift up and soothe? This, then, was the occasion on which the Lord appeared 
    in so glorious a form, with such soothing words and sublime revelations, to 
    His beloved servant. <br>
&nbsp; </p>
