    <p align="justify">MAY 29. </p>
    <p align="justify">&quot;Cast not away therefore your confidence, which has great 
    recompense of reward.&quot; Hebrews 10:35 </p>
    <p align="justify">There is nothing essentially omnipotent in any single 
    grace of the Spirit; to suppose this would be to deify that grace: although 
    regeneration is a spiritual work, and all the graces implanted in the soul 
    are the product of the Spirit, and must necessarily be in their nature 
    spiritual and indestructible, yet they may so decline in their power, become 
    so enfeebled and impaired in their vigor and tendency, as to be classed 
    among the &quot;things that are ready to die.&quot; It is preeminently so with faith; 
    perhaps there is no part of the Spirit's work more constantly and severely 
    assailed, and consequently more exposed to declension, than this. Shall we 
    look at the examples in God's word? We cite the case of Abraham, the father 
    of the faithful; beholding him, at God's command, binding his son upon the 
    altar, and raising the knife for the sacrifice, we unhesitatingly exclaim- 
    &quot;Surely never was faith like this! Here is faith of a giant character; 
    faith, whose sinews no trial scan ever relax, whose luster no temptation can 
    ever dim.&quot; And yet, tracing the history of the patriarch still further, we 
    find that very giant faith now trembling and yielding under a trial far less 
    acute and severe; he, who could surrender the life of his promised son- that 
    son through whose lineal descent Jesus was to come- into the hands of God, 
    could not intrust that same God with his own. We look at Job: in the 
    commencement of his deep trial we find him justifying God; messenger follows 
    messenger, with tidings of yet deeper woe, but not a murmur is breathed; and 
    as the cup, now full to the brim, is placed to his lips, how sweetly sounds 
    the voice of holy resignation,&quot; The Lord gave, and the Lord has taken away; 
    blessed be the name of the Lord.&quot; &quot;In all this did not Job sin with his 
    lips:&quot; and yet the very faith, which thus bowed in meekness to the rod, so 
    declined as to lead him to curse the day of his birth! We see David, whose 
    faith could at one time lead him out to battle with Goliath, now fleeing 
    from a shadow, and exclaiming- &quot;I shall one day perish by the hand of Saul.&quot; 
    And mark how the energy of Peter's faith declined, who at one period could 
    walk boldly upon the tempestuous sea, and yet at another could deny his 
    Lord, panic-struck at the voice of a little maid. Who will say that the 
    faith of the holiest man of God may not at one time greatly and sadly 
    decline? <br>
    But we need not travel out of ourselves for the evidence and the 
    illustration of this affecting truth: let every believer turn in upon 
    himself. What, reader, is the real state of your faith? is it as lively, 
    vigorous, and active as it was when you first believed? Has it undergone no 
    declension? Is the object of faith as glorious in your eye as He then was? 
    Are you not now looking at second causes in God's dealings with you, instead 
    of lifting your eye and fixing it on Him alone? What is your faith in 
    prayer?- do you come boldly to the throne of grace, asking, nothing 
    doubting? Do you take all your trials, your needs, your infirmities, to God? 
    What is your realization of eternal things- is faith here in constant, holy 
    exercise? Are you living as a pilgrim and a sojourner, &quot;choosing rather to 
    suffer affliction with the people of God,&quot; than float along on the summer 
    sea of this world's enjoyments? What is the crucifying power of your faith?- 
    does it deaden you to sin, and wean you from the world, and constrain you to 
    walk humbly with God, and near to Jesus? And when the Lord brings the cross, 
    and says, &quot;Bear this for Me,&quot; does your faith promptly and cheerfully 
    acquiesce, &quot;any cross, any suffering, any sacrifice for You, dear Lord&quot;? 
    Thus may you test the nature and the degree of your faith; bring it to the 
    touch-stone of God's truth, and ascertain what its character is, and how it 
    has suffered declension. <br>
&nbsp; </p>
