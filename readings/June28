    <p align="justify">JUNE 28. </p>
    <p align="justify">&quot;The path of the just is as the shining light, that 
    shines more and more unto the perfect day.&quot; Proverbs 4:18 </p>
    <p align="justify">The first light that dawns upon the soul is the daybreak 
    of grace. When that blessed period arrives, when the Sun of Righteousness 
    has risen upon the long-benighted mind, how do the shadows of ignorance and 
    of guilt instantly disappear! What a breaking away of, perhaps, a long night 
    of alienation from God, of direct hostility to God, and of ignorance of the 
    Lord Jesus, then takes place. Not, however, strongly marked is this state 
    always at the first. The beginning of grace in the soul is frequently 
    analogous to the beginning of day in the natural world. The dawn of grace is 
    at first so faint, the daybreak so gentle, that a skillful eye only can 
    observe its earliest tints. The individual himself is, perhaps, ignorant of 
    the extraordinary transition through which his soul is passing. The 
    discovery of darkness which that day-dawn has made, the revelation it has 
    brought to view of the desperate depravity of his heart, the utter 
    corruption of his fallen nature, the number and the turpitude of his sins, 
    it may be, well near overwhelms the individual with despair! But what has 
    led to this discovery? What has revealed all this darkness and sin? Oh! it 
    is the daybreak of grace in the soul! One faint ray, what a change has it 
    produced! And is it real? Ah! just as real as that the first beam, faintly 
    painted on the eastern sky, is a real and an essential part of light. The 
    daybreak, faint and glimmering though it be, is as really day as the 
    meridian is day. And so is it with the day-dawn of grace in the soul. The 
    first serious thought- the first real misgiving- the first conviction of 
    sin- the first downfall of the eye- the first bending of the knee- the first 
    tear- the first prayer- the first touch of faith, is as really and as 
    essentially the daybreak of God's converting grace in the soul as is the 
    utmost perfection to which that grace can arrive. Oh, glorious dawn is this, 
    my reader, if now for the first time in your life the daybreak of grace has 
    come, and the shadows of ignorance and guilt are fleeing away before the 
    advancing light of Jesus in your soul. If now you are seeing how depraved 
    your nature is; if now you are learning the utter worthlessness of your own 
    righteousness; if now you are fleeing as a poor, lost sinner to Christ, 
    relinquishing your hold of everything else, and clinging only to Him; though 
    this be but in weakness, and tremulousness, and hesitancy, yet sing for joy, 
    for the day is breaking- the prelude to the day of eternal glory- and the 
    shadows of unregeneracy are forever fleeing away. And as this day of grace 
    has begun, so it will advance. Nothing shall impede its course, nothing 
    shall arrest its progress. &quot;He which has begun a good work in you will 
    perform it until the day of Jesus Christ.&quot; The Sun, now risen upon you with 
    healing in His beams, shall never stand still- shall never go back. &quot;He has 
    set a tabernacle for the sun&quot; in the renewed soul of man; and onward that 
    sun will roll in its glorious orbit, penetrating with its beams every dark 
    recess, until all mental shadows are merged and lost in its unclouded and 
    eternal splendor. <br>
&nbsp; </p>
