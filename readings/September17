    SEPTEMBER 18.<br>
    <br>
    &quot;But we all with open face, beholding as in a glass the glory of the Lord, 
    are changed into the same image from glory to glory, even as by the Spirit 
    of the Lord.&quot; 2 Corinthians 3:18<br>
    <br>
    Is your knowledge of God a transforming knowledge? Have you so become 
    acquainted with God as to receive the impress (as it were) of what God 
    is?-for a true knowledge of God is a transforming knowledge. As I look upon 
    the glory of God I am changed into that glory; and as my acquaintance with 
    God deepens, I become more like God. There is a transfer of God's moral 
    image to my soul. Is your knowledge then transforming? Does your 
    acquaintance with God make you more like God-more holy, more divine, more 
    heavenly, more spiritual? Does it prompt you to pant after conformity to 
    God's mind, desiring in all things to walk so as to please God, and to have, 
    as it were, a transfer of the nature of God to your soul? Examine, 
    therefore, your professed acquaintance with God, and see whether it is that 
    acquaintance which will bring you to heaven, and will go on increasing 
    through the countless ages of eternity.<br>
    <br>
    And I would say to God's saints-trace the cause of much of our uneven 
    walking, of our little holiness, and, consequently, of our little happiness, 
    to our imperfect acquaintance with what God is. Did I know more of what God 
    is to me in Christ-how He loves me, what a deep interest He takes in all my 
    concerns-did I know that He never withdraws His eye from me for one moment, 
    that His heart of love never grows cold-oh! did I but know this, would I not 
    walk more as one acquainted with God? Would I not desire to consult Him in 
    all that interests me, to acknowledge Him in all my ways, to look up to Him 
    in all things, and to deal with Him in all matters? Would I not desire to be 
    more like Him, more holy, more divine, more Christ-like? Yes, beloved; it is 
    because we know Him so little, that we walk so much in uneven ways. We 
    consult man rather than God; we flee to the asylum of a creature-bosom, 
    rather than to the bosom of the Father; we go to the sympathy of man, rather 
    than to the sympathy of God in Christ, because we are so imperfectly 
    acquainted with God. But did I know more clearly what God is to me in the 
    Son of His love, I should say-I have not a trial but I may take that trial 
    to my Father; I am not in a perplexity but I may go to God for counsel; I am 
    in no difficulty, I have no want, but it is my privilege to spread it before 
    my Father-to unveil my heart of sin, my heart of wretchedness, my heart of 
    poverty, to Him who has unveiled His heart of love, His heart of grace, His 
    heart of tenderness to me in Christ. As I become more acquainted with God, 
    my character and my Christian walk will be more even, more circumspect, more 
    holy, and consequently more happy.<br>
    <br>
    <br>
