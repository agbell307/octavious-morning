    AUGUST 7.<br>
    <br>
    &quot;Peace I leave with you, my peace I give unto you: not as the world gives
    give I unto you.&quot; John 14:27<br>
    <br>
    Peace also is a fruit of spiritual-mindedness. What peace of conscience does
    that individual possess whose mind is stayed upon spiritual things! It is as
    much the reward as it is the effect of his cultivated heavenliness. The
    existence of this precious blessing, however, supposes the exposure of the
    spiritual mind to much that has a tendency to ruffle and disturb its
    equanimity and repose. The Christian is far from being entirely exempt from
    those chafings and disquietudes which seem inseparable from human life. To
    the br>ooding anxieties arising from external things- life's vicissitudes,
    mutations, and disappointments; there are added, what are peculiar to the
    child of God, the internal things that distract- the cloudings of guilt, the
    agitations of doubt, the corrodings of fear, the mourning of penitence, the
    discipline of love. <br>
    <br>
    But through all this there flows a river, the streams whereof make glad the
    city of God. It is the peace of the heavenly mind, the peace which Jesus
    procured, which God imparts, and which the Holy Spirit seals. A heavenly
    mind soars above a poor dying world, living not upon a creature's love or
    smile- casting its daily need upon the heart of a kind Providence- anxious
    for nothing, but with supplication and thanksgiving making known its
    requests unto God- indifferent to the turmoil, vexations, and chequered
    scenes of worldly life, and living in simple faith and holy pleasing on
    Christ. Thus detached from earth, and moving heavenwards by the attractions
    of its placid coast, it realizes a peace which passes all understanding. <br>
    <br>
    And if this be the present of the heavenly mind, what will be the future of
    the mind in heaven? Heaven is the abode of perfect peace. There are no
    cloudings of guilt, no tossings of grief, no agitations of fear, no
    corrodings of anxiety there. It is the peace of perfect purity- it is the
    repose of complete satisfaction. It is not so much the entire absence of all
    sorrow, as it is the actual presence of all holiness, that constitutes the
    charm and the bliss of future glory. <br>
    <br>
    The season of sorrow is frequently converted into that of secret joy- Christ
    making our very griefs to sing. But the occasion of sin is always that of
    bitter grief; our backslidings often, like scorpions, entwined around our
    hearts. Were there even- as most assuredly there will not be- sadness in
    heaven, there might still be the accompaniment of happiness; but were there
    sin in heaven- the shadow of a shade of guilt- it would becloud and embitter
    all. Thus, then, as heaven is the abode of perfect peace, he who on earth
    has his conversation most in heaven approximates in his feelings the nearest
    to the heavenly state. Oh that our hearts were more yielding to the sweet,
    holy, and powerful attractions of the heavenly world! Then would our
    conversation be more in heaven.<br>
    <br>
    <br>
