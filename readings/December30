    DECEMBER 30.<br>
    <br>
    &quot;You shall guide me with your counsel, and afterward receive me to glory.&quot; 
    Psalm 73:24<br>
    <br>
    LORD, give me more clearly to see Your love in all Your dealings. Anoint my 
    eye of faith afresh, that, piercing the dark cloud, it may observe beneath 
    it Your heart, all beating with an infinite and a deathless affection 
    towards me. The cup which my Father has prepared and given me, shall I not 
    drink in deep submission to His holy will? O Lord, I dare not ask that it 
    may pass my lips untasted: I may find a token of Your love concealed beneath 
    the bitter draught. Your will be done. Nearer would I be to You. And since 
    You, my blessed Lord, were a sufferer--Your sufferings now are all passed--I 
    would have fellowship with You in Your sufferings, and thus be made 
    conformable to Your death. Grant me grace, that patience may have her 
    perfect work, wanting nothing. Calm this perturbed mind. Tranquillize this 
    ruffled spirit. Bind up this bruised and broken heart. Say to these troubled 
    waters in which I wade, &quot;Peace, be still.&quot; Jesus, I throw myself upon Your 
    gentle bosom. To whom can I, to whom would I, tell my grief, to whom unveil 
    my sorrow, but to You? Lord! it is too tender for any eye, too deep for any 
    hand, but Your. I bless You that I am shut up to You, my God. &quot;Whom have I 
    in heaven but You? and there is none upon earth that I desire beside You.&quot; 
    You did hear my prayer, and have answered me, &quot;though as by fire.&quot; I asked 
    for health of soul, and You gave sickness of body. I asked You to possess my 
    entire heart, and You broke my idol. I asked that I might more deeply drink 
    of the fountain of Your love, and You did break my cistern. I asked to sit 
    beneath Your shadow with greater delight, and You smote my gourd. I asked 
    for deeper heart-holiness, and You did open to me more widely the chambers 
    of imagery. But it is well; it is all well. Though You do slay me, yet will 
    I trust in You. Divine and holy Comforter, lead me to Jesus, my comfort. 
    Witness to my spirit that I am a child of God, though an erring and a 
    chastened one. Lord! I come to You! My soul would sincerely expand her 
    wings, and fly to its home. Let me go, for the day breaks. Come to me, or 
    let me come to You. Ever with You, Lord, oh! that will be heaven indeed. Why 
    do Your chariot wheels so long tarry? Hasten, blessed Savior, and dissolve 
    my chain, and let me spring into glory, and see Your unclouded face, and 
    drink of the river of Your love, and drink--forever.<br>
    <br>
    <br>
