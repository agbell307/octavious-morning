    <p align="justify">MARCH 24. </p>
    <p align="justify">&quot;By one Spirit are we all baptized into one body, whether 
    we be Jews or Gentiles, whether we be bond or free; and have been all made 
    to drink into one Spirit.&quot; 1 Cor. 12:13. </p>
    <p align="justify">The Church of God is equally one in the Holy Spirit. One 
    Spirit regenerating all, fashioning all, teaching all, sealing all, 
    comforting all, and dwelling in all. Degrees of grace and &quot;diversities of 
    gifts&quot; there are, &quot;but the same Spirit.&quot; That same Spirit making all 
    believers partakers of the same divine nature, and then taking up his abode 
    in each, must necessarily assimilate them in every essential quality, and 
    feature, and attribute of the Christian character. Thus, the unity of the 
    Church is an essential and a hidden unity. With all the differences of 
    opinion, and the varieties of ceremonial, and the multiplicity of sects into 
    which she is broken and divided, and which tend greatly to impair her 
    strength, and shade her beauty, she is yet essentially and indivisibly one- 
    her unity consisting, not in a uniformity of judgment, but better far than 
    this, in the &quot;unity of the Spirit.&quot; Thus, no individual believer can with 
    truth say that he possesses the Spirit exclusively, boasting himself of what 
    other saints have not; nor can any one section of the Christian Church lay 
    claim to its being the only true Church, and that salvation is found only 
    within its pale. These lofty pretensions, these exclusive claims, this 
    vain-glory and uncharitableness, are all demolished by one lightning touch 
    of truth, even by that blessed declaration, &quot;For by one Spirit are we all 
    baptized into one body.&quot; <br>
&nbsp; </p>
