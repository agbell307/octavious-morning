    <p align="justify">FEBRUARY 7. </p>
    <p align="justify">&quot;When He, the Spirit of truth, has come, He will guide 
    you into all truth.&quot; John 16:13. </p>
    <p align="justify">New and enlarged views of the Holy Spirit mark a 
    regenerate mind. Having received the Holy Spirit as a quickener, he feels 
    the need of Him now as a teacher, a sanctifier, a comforter, and a sealer. 
    As a teacher, discovering to him more of the hidden evil of the heart, more 
    knowledge of God, of His word, and of His Son. As a sanctifier, carrying 
    forward the work of grace in the soul, impressing more deeply on the heart 
    the Divine image, and bringing every thought and feeling and word into 
    sweet, holy, and filial obedience to the law of Jesus. As a comforter, 
    leading him, in the hour of his deep trial, to Christ; comforting, by 
    unfolding the sympathy and tenderness of Jesus, and the exceeding 
    preciousness and peculiar fitness of the many promises with which the word 
    of truth abounds for the consolation of the Lord's afflicted. As a sealer, 
    impressing upon his heart the sense of pardon, acceptance, and adoption; and 
    entering himself as the &quot;earnest of the inheritance, until the redemption of 
    the purchased possession.&quot; Oh! what exalted views does he now have of the 
    blessed and eternal Spirit- of His personal glory, His work, His offices, 
    His influences, His love, tenderness, and faithfulness! The ear is open to 
    the softest whisper of His voice; the heart expands to the gentlest 
    impression of His sealing, sanctifying influence. Remembering that He is &quot;a 
    temple of the Holy Spirit,&quot; he desires so to walk- lowly, softly, 
    watchfully, and prayerfully. Avoiding everything that would &quot;grieve the 
    Spirit,&quot; resigning every known sin that would dishonor and cause Him to 
    withdraw, the one single aim of his life is to walk so as to please God, 
    that &quot;God in all things may be glorified.&quot; <br>
&nbsp; </p>
