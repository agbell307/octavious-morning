    DECEMBER 24.<br>
    <br>
    &quot;This Jesus has God raised up, whereof we all are witnesses. Therefore being 
    by the right hand of God exalted, and having received of the Father the 
    promise of the Holy Spirit, he has shed forth this, which you new see and 
    hear.&quot; Acts 2:32, 33<br>
    <br>
    THE day of Pentecost, with its hallowed scenes, cannot be too frequently 
    brought before the mind. Were there a more simple looking to Christ upon the 
    throne, and a stronger faith in the promise of the outpouring of the Spirit, 
    and in the faithfulness of the Promiser to make it good, that blessed day 
    would find its prototype in many a similar season enjoyed by the Church of 
    God to the end of time. The effects of the descent of the Spirit on that day 
    upon the apostles themselves are worthy of our especial notice. What a 
    change passed over those holy men of God, thus baptized with the promised 
    Spirit! A new flood of divine light broke in upon their minds. All that 
    Jesus had taught them while yet upon earth recurred to their memory, with 
    all the freshness and glory of a new revelation. The doctrines which He had 
    propounded concerning Himself, His work, and His kingdom, floated before 
    their mental eye like a newly-discovered world, full of light and beauty. A 
    newness and a freshness invested the most familiar truths. They saw with new 
    eyes; they heard with new ears; they understood as with recreated minds: and 
    the men who, while He was with them, teaching them in the most simple and 
    illustrative manner, failed fully to comprehend even the elementary 
    doctrines and the most obvious truths of the gospel, now saw as with the 
    strength of a prophet&rsquo;s vision, and now glowed as with the ardor of a 
    seraph&rsquo;s love. Upon the assembled multitudes who thronged the temple how 
    marvelous, too, the effects! Three thousand as in one moment were convinced 
    of sin, and led to plunge in the &quot;Fountain opened to the house of David and 
    the inhabitants of Jerusalem, for sin and uncleanness.&quot; And how does the 
    apostle explain the glorious wonder?--&quot;This Jesus,&quot; says he, &quot;has God raised 
    up, whereof we all are witnesses. Therefore being by the right hand of God 
    exalted, and having received of the Father the promise of the Holy Spirit, 
    he has shed forth this which you now see and hear.&quot;<br>
    <br>
    This, and this only, is the blessing which the Church of God now so greatly 
    needs--even the baptism of the Holy Spirit. She needs to be confirmed in the 
    fact, that Jesus is alive and upon the throne, invested with all power, and 
    filled with all blessing. The simple belief of this would engage her heart 
    to desire the bestowment of the Spirit; and the Spirit largely poured down 
    would more clearly demonstrate to her the transcendent truth in which all 
    her prospects of glory and of happiness are involved, that the Head of the 
    Church is triumphant. Oh, let her but place her hand of faith simply, 
    solely, firmly, on the glorious announcement--Jesus is at the right hand of 
    the Father, with all grace and love in His heart, with all authority in His 
    hand, with all power at His disposal, with all blessing in His gift, waiting 
    to open the windows of heaven, and pour down upon her such a blessing as 
    there shall not be room enough to receive it--prepared so deeply to baptize 
    her with the Holy Spirit as shall cause her converts greatly to increase, 
    and her enterprises of Christian benevolence mightily to prosper; as shall 
    heal her divisions, build up her broken walls, and conduct her to certain 
    and triumphant victory over all her enemies--let her but plant her faith upon 
    the covenant and essential union of these two grand truths--An exalted 
    Redeemer and a descending Spirit--and a day on which, not three thousand 
    only, but a nation shall turn to the Lord, and all flesh shall see His 
    glory!<br>
    <br>
    <br>
