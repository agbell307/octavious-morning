    <p align="justify">JANUARY 18. </p>
    <p align="justify">&quot;Mighty to save.&quot; Isaiah 63:1. </p>
    <p align="justify">Let us glance at the authoritative manner with which He 
    executes His mighty acts of grace. Mark His deportment. Was there anything 
    that betrayed the consciousness of an inferior, the submission of a 
    dependant, the weakness of a mortal, or the imperfection of a sinner? Did 
    not the God shine through the man with majestic awe, when to the leper He 
    said, &quot;I will, be clean;&quot;- to the man with the withered hand, &quot;Stretch forth 
    your hand;&quot;- to the blind man, &quot;Receive your sight;&quot;- to the dead man, &quot;I 
    say unto you, Arise;&quot;- and to the tumultuous waves, &quot;Peace, he still&quot;? Dear 
    reader, are you an experimental believer in Jesus? Then this omnipotent 
    Christ is wedded to your best interests. He is omnipotent to save- 
    omnipotent to protect- omnipotent to deliver- omnipotent to subdue all your 
    iniquities, to make you humble, holy, and obedient. All power resides in 
    Him. &quot;It pleased the Father that in Him&quot;- in Him as the Mediator of His 
    Church- &quot;all fullness should dwell.&quot; Not a corruption, but He is omnipotent 
    to subdue it: not a temptation, but He is omnipotent to overcome it: not a 
    foe, but He is omnipotent to conquer it: not a fear, but He is omnipotent to 
    quell it. &quot;All power,&quot; is His own consoling language, &quot;all power is given 
    unto Me in heaven and in earth.&quot; <br>
&nbsp; </p>
