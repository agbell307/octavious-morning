    SEPTEMBER 17.<br>
    <br>
    &quot;This is life eternal, that they might know you, the only true God, and 
    Jesus Christ, whom you have sent.&quot; John 17:3<br>
    <br>
    WHEN does this acquaintance between God and man commence? It commences in 
    reconciliation-it commences at the time of man's peace with God. I can form 
    no acquaintance with an individual against whom my heart cherishes deep, 
    inveterate, and deadly enmity; my very hatred, my very dislike to that 
    individual prevents me from studying his character, from analyzing his 
    heart, and from knowing what are his feelings towards me. But bring me into 
    a state of amity with that individual-remove my enmity, take away my 
    dislike, propitiate his feelings towards me, and then I am in a position for 
    studying and becoming acquainted with his character. The Holy Spirit does 
    this in man; He takes away the enmity of the sinner's heart, humbles his 
    spirit, and bows it in penitence; constrains the sinner to lay down the 
    weapons of his hostility against God-brings him to see that the God against 
    whom he has been battling and fighting all his life is a God of love, a God 
    who draws sinners to Himself, a God who is reconciled in Jesus Christ. That 
    soul, disarmed of its rebellion and enmity, is now brought into a position 
    for the study of God's character. Looking at God now, not through the law, 
    but through the gospel, not in creation, but in Christ, he is in a position 
    for becoming acquainted with God. And oh what an acquaintance he now forms! 
    All his dark and shadowy conceptions vanish away; all his distorted views 
    are rectified; and the God that he thought was a God so hateful, a God whose 
    law was so repulsive, a God who was so harsh and tyrannical, he sees now to 
    be a God of infinite mercy and love in Jesus Christ: now he becomes 
    acquainted with Him as a sin-pardoning God, blotting out the utmost remnant 
    of his transgressions; he becomes acquainted with Him as a God reconciled in 
    Christ, and therefore a Father pacified towards him. Oh! what a discovery is 
    made to him of that God, with whom before his soul lived in the darkest and 
    deepest alienation! Thus he becomes acquainted with God, when his heart 
    becomes reconciled to God. A closer and more simple view of Jesus, a daily 
    study of Jesus, must deepen my acquaintance with God. As I know more of the 
    heart of Christ, I know more of the heart of the Father; as I know more of 
    the love of the Savior, I know more of the love of Him who gave me that 
    Savior; as I know more of His travail of soul, to work out my redemption-as 
    I know more of the tears of blood He shed-as I know more of the groans of 
    agony He breathed-as I know more of the convulsions through which He 
    passed-as I know more of the death-throes of the spotless soul of His-I know 
    more of the heart of God, more of the character of God, and more of the love 
    of God. Want you to see more of the glory of God? See it in the face of 
    Jesus. Learn it in the &quot;brightness of the Father's glory,&quot; learn it in &quot;the 
    express image of His person,&quot; as it stands revealed to you in the person and 
    in the work of Jesus Christ.<br>
    <br>
    <br>
