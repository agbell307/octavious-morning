    AUGUST 29.<br>
    <br>
    &quot;But he that received the seed into stony places, the same is he that hears
    the word, and anon with joy receives it; yet has he no root in himself, but
    endures for awhile: for when tribulation or persecution arises because of
    the word, by and by he is offended. He also that received seed among the
    thorns is he that hears the word; and the care of this world, and the
    deceitfulness of riches, choke the word, and he becomes unfruitful. Matthew
    13:20-22<br>
    <br>
    &quot;The rocky soil represents those who hear the message and receive it with
    joy. But like young plants in such soil, their roots don't go very deep. At
    first they get along fine, but they wilt as soon as they have problems or
    are persecuted because they believe the word. The thorny ground represents
    those who hear and accept the Good News, but all too quickly the message is
    crowded out by the cares of this life and the lure of wealth, so no crop is
    produced.&quot; Matthew 13:20-22<br>
    <br>
    A season of prosperity often proves fatal to a profession of godliness.
    Divine providence smiles, riches increase, and with them the temptations and
    the snares, the luxury, indulgence, and worldly show which are inseparable
    from the accumulation of unsanctified and unconsecrated wealth. And what are
    the results? In most cases, the entire relinquishment of the outward garb of
    a religious costume. Found to be in the way of the full indulgence of the
    carnal mind, it is laid aside altogether; and thus freed from all the
    restraints which consistency imposed, the heart at once plunges deep into
    the world it all the while secretly loved, sighed for, and worshiped. Oh,
    what a severe but true test of religious principle is this! How soon it
    detects the spurious and the false! How soon does the verdure wither away!
    &quot;The prosperity of fools shall destroy them.&quot; <br>
    <br>
    But if a professing man passes through this trial, and still retains his
    integrity; still walks closely and humbly with God; still adheres to the
    lowly cross-bearing path of Jesus; is still found as diligent in waiting
    upon God in public and private means of grace; is still as meek,
    condescending, and kind, increasing in devotedness, liberality, and love,
    with the increase of God's providential goodness around him, such a man has
    the &quot;root of the matter in him;&quot; and &quot;he shall be like a tree planted by the
    rivers of water, that br>ings forth his fruit in his season; his leaf also
    shall not wither; and whatever he does shall prosper.&quot; His prosperity has
    not destroyed him. <br>
    <br>
    A time of adversity is often equally as fatal to a profession of religion,
    founded upon no true Christian principle. If in the smooth path we are apt
    to slide, in the rough path we may stumble. Periods of great revolution in
    the history of the Christian Church, when God tries the principles, the
    conscience, the love, and the faith of His people, are test-periods. What
    numbers make shipwreck then of their high profession! And when God enters
    the pleasant garden of a man's domestic blessings, and blows upon the lovely
    blossom, or blights the fair flower, or severs the pleasant bough, or
    scatters the hard-earned wealth of years, or wastes the body's vigor, or
    frustrates the fond scheme; how does an unrenewed man behave himself? <br>
    <br>
    Is his carriage humble, submissive, child-like? Does stern Christian
    principle now exhibit itself, in beautiful contrast with the trial that has
    called it forth? Does divine grace, like the aromatic flower, now appear the
    sweeter and more precious for its being crushed? Does not every feeling of
    the heart rise in maddened rebellion against God and against His government?
    Ah, yes! how accurately does Christ describe his case: &quot;he has not root in
    himself, but endures for a while; for when tribulation or persecution arises
    because of the word, by and by he is offended.&quot;<br>
    <br>
    <br>
