    DECEMBER 29.<br>
    <br>
    &quot;For you are my lamp, O Lord; and the Lord will lighten my darkness.&quot; 2 
    Samuel 22:29<br>
    <br>
    BLESSED Lord! You are my light. Accepted in Your righteousness, I am 
    &quot;clothed with the sun.&quot; Dark in myself, I am light in You. Often have You 
    turned my gloomy night into sunny day. Yes, Lord, and with a love not less 
    tender, You have sometimes turned my &quot;morning of joy&quot; into a &quot;night of 
    weeping.&quot; Yet have You made my very griefs to sing. Many a dark cloud of my 
    pilgrimage has You fringed with Your golden beams. &quot;In Your light have I 
    seen light&quot; upon many a gloomy and mysterious dispensation of my covenant 
    God. &quot;By Your light I have walked through darkness,&quot; many a long and lonely 
    stage of my journey. Oh, how have You gone before me each step You do bid me 
    to travel. You, too, did pass through Your night of solitude, suffering, and 
    woe. But You were deprived of the alleviations which You do so graciously 
    and tenderly vouchsafe to me. Not a beam illumined, not a note cheered, the 
    midnight of Your soul. The light of the manifested Fatherhood was hidden 
    from Your view, and in bitter agony did You exclaim, &quot;My God, my God, why 
    have You forsaken me?&quot; And all this did You willingly endure, that I might 
    have a song in the night of my grief. Thus Your darkness becomes my light; 
    Your suffering my joy; Your humiliation my glory; Your death my life; Your 
    curse my crown.<br>
    <br>
    O Lord! that is a blessed night of weeping in which I can sing of Your 
    sustaining grace, of Your enlivening presence, of Your unfaltering 
    faithfulness, of Your tender love. In Your school how well have You 
    instructed me! How patiently and skillfully have You taught me! I could not 
    have done without Your teaching and Your discipline. With not one night of 
    suffering, with not one chastising stroke, with not one ingredient in my cup 
    of sorrow, could I safely have dispensed. All was needful. And now I can 
    see, as faith, with a reflex action, surveys all the past, with what 
    infinite wisdom and skill, integrity and gentleness, You were appointing 
    all, and overruling all the incidents and windings of my history. With not 
    less shame and self-abhorrence do I cover my face, and lay my mouth in the 
    dust before You, because You has brought light out of my darkness, and 
    educed good from my evil, and overruled all my mistakes and departures for 
    my greater advance and Your richer glory, and are now &quot;pacified towards me 
    for all that I have done.&quot; I have stumbled, and You have upheld me. I have 
    fallen, and You have raised me up. I have wandered, and You have restored. I 
    have wounded myself, and You have healed me. Oh, what a God have You been to 
    me! What a Father! What a Friend! Shall I ever distrust You, ever disbelieve 
    You, ever wound You, ever leave You more? Ah! Lord, a thousand times over, 
    yes, this very moment, but for Your restraining grace. &quot;Hold You me up, and 
    I shall be safe.&quot;<br>
    <br>
    <br>
