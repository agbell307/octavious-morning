    JULY 14.<br>
    <br>
    &quot;And if any man sins, we have an advocate with the Father, Jesus Christ the 
    righteous.&quot; 1 John 2:1<br>
    <br>
    The work of our Lord as Priest was two-fold, atonement and intercession. The 
    one He accomplished upon the cross, the other He now transacts upon the 
    throne. &quot;When He had by Himself purged our sins, He sat down on the right 
    hand of the Majesty on high.&quot; The high priest, under the law, after that he 
    had slain the sacrifice, took the blood, and, passing within the veil, 
    sprinkled it on the mercy-seat, so making intercession for the people. &quot;The 
    Holy Spirit this signifying, that the way into the holiest of all was not 
    yet made manifest, while as the first tabernacle was yet standing.&quot; &quot;But, 
    Christ being come, an high priest of good things to come, by a greater and 
    more perfect tabernacle, not made with hands, that is to say, not of this 
    building; neither by the blood of goats and calves, but by His own blood, He 
    entered in once into the holy place, having obtained eternal redemption for 
    us.&quot; <br>
    <br>
    And what is He now doing? Presenting His own blood each moment before the 
    mercy-seat on behalf of His redeemed people! &quot;He ever lives to make 
    intercession for us.&quot; Oh, do not forget this, dear saint of God! This is 
    spoken for the comfort of the mourners in Zion-for those who, knowing the 
    plague of their own hearts, and deploring its constant tendency to outbreak, 
    are humbled in the dust with deep godly sorrow. Look up! Does sin plead 
    loudly against you? the blood of Jesus pleads louder for you. Do your 
    backslidings, and rebellions, and iniquities, committed against so much 
    light and love, call for vengeance? the blood of Jesus &quot;speaks better 
    things.&quot; Does Satan stand at your right hand to accuse you? your Advocate 
    stands at God's right hand to plead for you. All hail! you mourning souls! 
    you that smite on the breast, you broken-hearted, you contrite ones! &quot;who is 
    he that condemns! It is Christ who died, yes rather, who is risen again; who 
    is even at the right hand of God, who also makes intercession for us.&quot;<br>
    <br>
    Jesus is a glorious and a successful Advocate. He has never lost a cause 
    entrusted to His advocacy, and never will. He pleads powerfully, He pleads 
    eloquently, He pleads prevalently, because He pleads in behalf of a people 
    unspeakably dear to His heart, for whom He &quot;loved not His own life unto the 
    death,&quot; and presses His suit, on the ground of His own most precious blood 
    and accepted person, and with His father and their Father, His God and their 
    God.<br>
    <br>
    <br>
