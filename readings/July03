    JULY 3.<br>
    <br>
    &quot;Therefore, thus says the Lord God, Behold, I lay in Zion for a foundation a 
    stone, a tried stone.&quot; Isaiah 28:16<br>
    <br>
    Jesus is fitly compared to a &quot;stone&quot; for strength and durability. He is a 
    &quot;Savior, and a great one&quot;-&quot;mighty to save.&quot; &quot;I have laid help upon one that 
    is mighty.&quot; If it were probable that the fact of His Deity should be 
    announced in a voice of thunder from the eternal throne, can we suppose it 
    would be uttered in terms more decided and explicit than those which fell 
    upon the ear of the exiled evangelist from the lips of Christ Himself? &quot;I am 
    Alpha and Omega, the beginning and the ending, says the Lord, who is, and 
    who was, and who is to come, the Almighty.&quot; And what a needed truth is this! 
    None but an almighty ransom could have saved from going down to the pit. 
    Jesus is our ransom, and Jesus is the Almighty.<br>
    <br>
    The Redeemer is not only a stone, but a &quot;tried stone.&quot; The grand experiment 
    has been made-the great test has been applied, and to answer all the ends 
    for which the Lord God laid it in Zion, it has proved completely adequate. 
    Never was a foundation tried as this. In the eternal purpose of redemption, 
    Omnipotence tried it. In the Divine mind there existed no lurking suspicion, 
    no embarrassing uncertainty as to the result. The Father knew all that this 
    foundation was to sustain, and well He knew, too, that it was capable of 
    sustaining all. Stupendous were the consequences. His own glory and the 
    honor of His government were involved; the salvation of His elect was to be 
    secured; death, with all its horrors, was to be abolished; life, with all 
    its immortal, untold glories, was to be revealed; hell was to be closed, and 
    heaven opened to all believers. With such momentous realities pending, with 
    such mighty and glorious results at stake, the Eternal mind, in its purpose 
    of grace and glory, would lay for a foundation a &quot;tried stone.&quot; Blessed 
    Emmanuel! how effulgently does Your glory beam from beneath Your prophetical 
    veil! You are that &quot;tried stone,&quot;-tried by the Father, when He laid upon You 
    all His people's sins and transgressions, bruised You, and put You to grief. 
    Tried by the law, when it exacted and received from You Your utmost 
    obedience to its precepts. Tried by Divine justice, when it kindled around 
    You its fiercest flame, yet consumed You not. Tried by the Church, built 
    upon You so securely that the gates of hell shall never prevail against her. 
    Tried by poor sinners, who have brought their burdens of guilt to Your 
    blood, and have found pardon and peace. Tried by believers, who have taken 
    their trials to Your sympathy, their sorrows to Your love, their wounds to 
    Your healing, their weakness to Your strength, their emptiness to Your 
    fullness, their petitions to Your ear, and have never, never been 
    disappointed. Oh yes, You are that &quot;tried stone&quot; to whom I would come moment 
    by moment.<br>
    <br>
    <br>
