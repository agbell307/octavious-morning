    <p align="justify">JUNE 8. </p>
    <p align="justify">&quot;Now the just shall live by faith.&quot; Hebrews 10:38 </p>
    <p align="justify">We cannot too frequently nor too deeply study the 
    profound meaning of these words. God will have his child perpetually looking 
    to, leaning upon, and receiving from Him. At present we are but in an 
    immature state. We are not, therefore, in a condition to be trusted with 
    grace for the future. Improvident and careless, we would soon squander and 
    exhaust our resources; and when the emergency came, we should find our 
    selves unprepared to meet it. The Lord, in wisdom and love, keeps all our 
    grace in His own hands, and deals it out just as our circumstances demand. 
    Oh, who that knows his own heart, and the heart of Christ, would not desire 
    that all his supply should be in God, and not in himself? Who, so to speak, 
    would wish to be his own spiritual treasurer? Who that knows the blessedness 
    of a life of faith, the sweetness of going to God in everything, and for 
    everything, would wish to transfer his mercies from Christ's keeping to his 
    own, or wish to hold in the present the supply of the future? Be satisfied, 
    dear reader, to walk by faith, and not by sight. You have a full Christ to 
    draw from, and a faithful God to look to. You have a &quot;covenant ordered in 
    all things and sure,&quot; and the precious promise, &quot;As your days, so shall your 
    strength be,&quot; to lean confidently upon all your journey through. Be content, 
    then, to be poor and dependent. Be willing to travel on empty-handed, seeing 
    God's heart opened, and Christ's hand outstretched to supply your daily 
    bread. Oh! it is sweet to be a dependent creature upon God- to hang upon a 
    loving Father- to live as a poor, needy sinner, day by day, moment by 
    moment, upon Jesus- to trace God in ten thousand ways- to mark His wisdom 
    here, His condescension there- now His love, and then His faithfulness, all 
    combining and exerted for our good- truly it is the most holy and blessed 
    life upon earth. Why should we, then, shrink from any trial, or flee from 
    any duty, or turn aside from any cross, since for that trial, and for that 
    duty, and for that cross, Jesus has provided its required and appropriate 
    grace? You are perhaps exclaiming, &quot;Trouble is near!&quot; Well, be it so. So 
    also Divine grace is near- and strength is near - and counsel is near- and 
    deliverance is near- and Jesus is near- and God is near- and a throne of 
    grace is near; therefore, why must you fear, though trouble be near? &quot;God is 
    our refuge and strength, a very present help in trouble.&quot; <br>
&nbsp; </p>
