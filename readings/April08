    <p align="justify">APRIL 8. </p>
    <p align="justify">&quot;This is my infirmity.&quot; Psalm 77:10. </p>
    <p align="justify">The infirmities of the believer are as varied as they are 
    numerous. Some are weak in faith, and are always questioning their interest 
    in Christ. Some, superficial in knowledge, and shallow in experience, are 
    ever exposed to the crudities of error and to the assaults of temptation. 
    Some are slow travelers in the divine life, and are always in the rear; 
    while yet others are often ready to halt altogether. Then there are others 
    who groan beneath the burden of bodily infirmity, exerting a morbid 
    influence upon their spiritual experience. A nervous temperament-&nbsp; a 
    state of perpetual depression and despondency- the constant corrodings of 
    mental disquietude- physical ailment- imaginary forebodings- a facile 
    yielding to temptation- petulance of spirit- unguardedness of speech- gloomy 
    interpretations of providence- an eye that only views the dark hues of the 
    cloud, the somber shadings of the picture. Ah! from this dismal catalogue 
    how many, making their selection, may exclaim, &quot;This is my infirmity.&quot; But 
    be that infirmity what it may, let it endear to our hearts the grace and 
    sympathy of Him who for our sake was encompassed with infirmity, that He 
    might have compassion upon those who are alike begirt. All the fulness of 
    grace that is in Jesus is for that single infirmity over which you sigh. </p>
