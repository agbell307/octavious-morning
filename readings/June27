    <p align="justify">JUNE 27. </p>
    <p align="justify">&quot;It is good that a man should both hope and quietly wait 
    for the salvation of the Lord.&quot; Lamentations 3:26 </p>
    <p align="justify">A believer may present a right petition in a right way, 
    and yet he may not wait the Lord's answer in His own time. He may appoint a 
    time, and if the Lord does not answer within that period, he turns away, 
    resigning all expectation of an answer. There is such a thing as waiting for 
    the Lord. The apostle alludes to and enjoins this holy patience, when he 
    speaks to the Ephesians of &quot;praying always with all prayer and supplication 
    in the Spirit, and watching thereunto with all perseverance.&quot; A believer may 
    present his request- may have some degree of nearness in urging it- may 
    press it with fervency- and yet, forgetting the hoping, quiet, waiting 
    patience which ought invariably to mark a praying soul; he may lose the 
    blessing he has sought. There is such a thing as &quot;waiting upon the Lord.&quot; 
    Oh; how long have we made Him to wait for us! For years, it may be, we kept 
    Him knocking, and standing, and waiting at the door of our hearts, until His 
    own Spirit took the work in His own hands, and unlocked the heart, and the 
    Savior entered. The Lord would now often have us wait His time in answering 
    prayer. And, if the vision tarry, still let us wait, and hope, and expect. 
    Let the delay but stimulate hope, and increase desire, exercise faith, and 
    multiply petitions at the mercy-seat. It will come when the Lord sees best.
    <br>
    A believer may lose the answer to his prayer, by dictating to the Lord the 
    mode, as well as the time, of answering. The Lord has His own mode of 
    blessing His people. We may prescribe the way the Lord should answer, but He 
    may send the blessing to us through an opposite channel, in a way we never 
    thought of, and should never have selected. Sovereignty sits regent upon the 
    throne, and in no aspect is its exercise more manifestly seen than in 
    selecting the way and the means by which the prayers of the saints of God 
    are answered. Dictate not to the Lord. If you ask a blessing through a 
    certain channel, or in a prescribed way, let it be with the deepest humility 
    of mind, and with perfect submission to the will of God. Be satisfied to 
    receive the blessing in any way which a good and covenant God may appoint. 
    Be assured, it will be in that way that will most glorify Himself, and 
    secure to you the greatest amount of blessing. <br>
&nbsp; </p>
