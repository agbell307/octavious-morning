    OCTOBER 11<br>
    <br>
    &quot;Who is among you that fears the Lord, that obeys the voice of his servant, 
    that walks in darkness, and has no light? let him trust in the name of the 
    Lord, and stay upon his God.&quot; Isaiah 50:10<br>
    <br>
    HOW prone is the believer to attach an undue importance to the mere article 
    of comfort! to give place to the feeling that when comfort vanishes, all 
    other good vanishes with it-thus, in fact, making the real standing of the 
    soul to depend upon an ever-fluctuating emotion. But let it be remembered 
    that the comfort of grace may be suspended, and yet the existence of grace 
    may remain; that the glory of faith may be beclouded, and yet the principle 
    of faith continue. Contemplate, as affording an illustrious example of this, 
    our adorable Lord upon the cross. Was there ever sorrow like His sorrow? Was 
    there ever desertion like His desertion? Every spring of consolation was 
    dried up. Every beam of light was beclouded. All sensible joy was withdrawn. 
    His human soul was now passing through its strange, its total eclipse. And 
    still His faith hung upon God. Hear Him exclaim, &quot;My God! my God!&quot; My strong 
    One! my strong One! His soul was in the storm-and oh, what a storm was 
    that!-but it was securely anchored upon His Father. There was in His case 
    the absence of all consolation, the suspension of every stream of comfort; 
    and yet in this, the darkest cloud that ever enshrouded the soul, and the 
    deepest sorrow that ever broke the heart, He stayed His soul upon God.<br>
    <br>
    And why should the believer, the follower of Christ, when sensible comfort 
    is withdrawn, &quot;cast away his confidence, which has great recompense of 
    reward&quot;? Of what use is the anchor but to keep the vessel in the tempest? 
    What folly were it in the mariner to weigh his anchor, or to slip his cable, 
    when the clouds gather blackness and the waves swell high! Then it is he 
    most needs them both. It is true he has cast his anchor into the deep, and 
    the depth hides it from his view; but though he cannot discern it through 
    the foaming waves, still he knows that it is firmly fastened, and will keep 
    his storm-tossed vessel from stranding upon a lee shore. And why should the 
    believer, when &quot;trouble is near,&quot; and sensible comfort is withdrawn, resign 
    his heart a prey to unbelieving fears, and cherish in his bosom the dark 
    suspicion of God? Were not this to part with the anchor of his hope at the 
    very moment that he the most needed it? I may not be able to pierce the 
    clouds and look within the veil with an eye beaming with an undimmed and 
    assured joy, but I know that the Forerunner is there; that the Priest is 
    upon His throne; that Jesus is alive, and is at the right hand of God-then 
    all is safe: faith demands, hope expects, and love desires no more.<br>
    <br>
    <br>
