    <p align="justify"><b>MARCH 1. </b></p>
    <p align="justify">&quot;Search the scriptures; for in them you think you have 
    eternal life: and they are they which testify of me.&quot; John 5:39 </p>
    <p align="justify">The word of God is full of Christ. He is the Sun of this 
    divine system, the Fountain of its light and beauty. Every doctrine derives 
    its substance from His person, every precept its force from His work, every 
    promise its sweetness from His love. Is it not to be feared, that in the 
    study of the Scriptures it is a much-forgotten truth, that they testify of 
    Jesus? Are they not read, searched, and examined, with a mind too little 
    intent upon adding to its wealth by an increased knowledge of His person, 
    and character, and work? And thus it is we lower the character of the Bible. 
    We may read it as a mere uninspired record; we may study it as a book of 
    human literature. Its antiquity may interest us, its history may inform us, 
    its philosophy may instruct us, its poetry may charm us; and thus, while 
    skimming the surface of this Book of books, the glorious Christ, who is its 
    substance, its subject, its sweetness, its worth- and but for whom there had 
    been no Bible- has been deeply and darkly veiled from the eye. <br>
    But it is the office of the blessed and eternal Spirit to unfold, and so to 
    glorify, Jesus in the Word. All that we spiritually and savingly learn of 
    Him, through this revealed medium, is by the sole teaching of the Holy 
    Spirit, opening up this word to the mind. He shows how all the luminous 
    lines of Scripture truth emanate from, return to, and center in, Christ- how 
    all the doctrines set forth the glory of His person, how all the promises 
    are written in His heart's blood, and how all the precepts are embodied in 
    His life. <br>
&nbsp; </p>
