    JULY 25.<br>
    <br>
    &quot;I lie in the dust, completely discouraged; revive me by your word.&quot; Psalm 
    119:25<br>
    <br>
    The argument with which this holy petition is urged is most powerful and 
    prevalent. According to the promise of the word, and the instrumentality of 
    the word. Both are engaged to quicken the soul. The promise is most 
    precious: &quot;I will heal their backslidings, I will love them freely; for my 
    anger is turned away from him. I will be as the dew unto Israel: he shall 
    grow as the lily, and cast forth his roots as Lebanon. Those who dwell under 
    his shadow shall return; they shall revive as the corn, and grow as the 
    vine.&quot; This precious promise to quicken and revive you, to shed the dews of 
    His grace upon your soul, thus moistening and nourishing the roots and 
    fibers and fruits of the new and heavenly life within you, God stands ready 
    to fulfill in your holy and happy experience. &quot;I will be to Israel like a 
    refreshing dew from heaven. It will blossom like the lily; it will send 
    roots deep into the soil like the cedars in Lebanon.&quot; Christ is our dew: the 
    dew of His love, the dew of His grace, the dew of His Spirit, is prepared, 
    silent and unseen, but effectual and vivifying-to fall upon the renewed 
    powers of your nature-reviving the work of God in your soul. <br>
    <br>
    But by the instrumentality of the word, the Lord quickens the soul. The word 
    of Christ is &quot;spirit and life,&quot; therefore it is a quickening word. &quot;This is 
    my comfort in my affliction; for Your word has quickened me.&quot; Again, &quot;I will 
    never forget Your precepts; for with them You have quickened me.&quot; Therefore 
    did Jesus pray to His Father in behalf of His Church, &quot;Sanctify them through 
    Your truth.&quot; Thus does the word quicken. We are here constrained to suggest 
    an inquiry-May not the prevalent decay of spiritual life in the Church of 
    God-the low standard of spirituality-the alarming growth of soul-destroying 
    error-the startling discovery which some modern teachers appear to have 
    stumbled upon, that doctrines which the Church of Christ has always received 
    as revealed truth, which councils have authorized, and which creeds have 
    embodied, and which the sanctified intellects of master-spirits-the Anakims 
    and the Shamgars of polemic divinity and divine philosophy of past ages-have 
    contended for and maintained, are not found in the Bible, but are the 
    visionary dogmata of a by-gone age-we say, may not these prevalent evils be 
    mainly attributable to the contempt thrown upon the word of God? We verily 
    and solemnly believe it to be so. <br>
    <br>
    We need to be constantly reminded that the great regenerator and emancipator 
    of the world is the Bible-that nothing short of this disturbs the spiritual 
    death which universally prevails, and that nothing short of this will free 
    the human mind from the shackles of error and superstition which enslave at 
    this moment nearly two-thirds of the human race. This &quot;sword of the 
    Spirit&quot;-like that of Goliath, &quot;there is none like it&quot;-has overcome popery 
    and infidelity, and, unimpaired by the conflict, it is ready to overcome 
    them yet again. Oh, that in this day of sad departure from the word of God, 
    we may rally round the Bible in closer and more united phalanx! Firm in the 
    belief of its divinity, strong in the conviction of its potency, may we go 
    forth in the great conflict of truth and error, wielding no weapon but the 
    &quot;sword of the Spirit, which is the word of God.&quot; In all our spiritual 
    relapses, too, may the word of the Lord quicken us. May it, like a mighty 
    lever, raise our soul from the dust to which it so much cleaves.<br>
    <br>
