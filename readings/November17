    NOVEMBER 17.<br>
    <br>
    &quot;To the praise of the glory of his grace, wherein he has made us accepted in 
    the beloved.&quot; Ephesians 1:6<br>
    <br>
    THE holy influence which a believer is called to exert around him will be 
    greatly augmented, and powerfully felt, by an abiding realization of his 
    full and entire acceptance in Christ. The child of God is &quot;the salt of the 
    earth,&quot; &quot;the light of the world,&quot; surrounded by moral putrefaction and 
    darkness. By his holy consistent example, he is to exert a counteracting 
    influence. He is to be purity where there is corruption, he is to be light 
    where there is darkness. And if his walk is consistent, if his life is holy, 
    his example tells, and tells powerfully, upon an ungodly world. Saints of 
    God catch, as it were, the contagion of his sanctity. The worldling 
    acknowledges the reality of the gospel he professes, and the bold skeptic 
    falls back abashed, and feels &quot;how awful goodness is!&quot; What, then, will so 
    elevate his own piety, and increase the power of his influence, as a 
    realization of his justification by Christ? Oh how this commends the 
    religion of Jesus! We will suppose a Christian parent surrounded by a large 
    circle of unconverted children. They look to him as to a living gospel: they 
    look to him for an exemplification of the truth he believes: they expect to 
    see its influence upon his principles, his temper, his affections, his whole 
    conduct. What, then, must be their impression of the gospel, if they behold 
    their parent always indulging in doubts as to his acceptance, yielding to 
    unbelieving fears as to his calling? Instead of walking in the full 
    assurance of faith, saying with the apostle, &quot;I know whom I have 
    believed&quot;--instead of living in the holy liberty, peace, and comfort of 
    acceptance, there is nothing but distrust, dread, and tormenting fear. How 
    many a child has borne this testimony, &quot;the doubts and fears of my parent 
    have been my great stumbling-block&quot;! Oh, then, for the sake of those around 
    you--for the sake of your children, your connections, your friends, your 
    domestics--realize your full, free, and entire acceptance in Christ.<br>
    <br>
    Is it any marvel, then, that in speaking of His beloved and justified 
    people, God employs in His word language like this: &quot;You are all fair, my 
    love: there is no spot in you.&quot; &quot;He has not beheld iniquity in Jacob, 
    neither has He seen perverseness in Israel&quot;? Carry out this thought. Had 
    there been no iniquity in Jacob? had there been no perverseness in Israel? 
    Read their histories, and what do they develop but iniquity and perverseness 
    of the most aggravated kind? And yet, that God should say He saw no iniquity 
    in Jacob, and no perverseness in Israel, what does it set forth but the 
    glorious work of the adorable Immanuel--the glory, the fitness, the 
    perfection of that righteousness in which they stand &quot;without spot, or 
    wrinkle, or any such thing&quot;? In themselves vile and worthless, sinful and 
    perverse, deeply conscious before God of possessing not a claim upon His 
    regard, but worthy only of His just displeasure, yet counted righteous in 
    the righteousness of another, fully and freely justified by Christ. Is this 
    doctrine startling to some? Is it considered too great a truth to be 
    received by others? Any other gospel than this, we solemnly affirm, will 
    never save the soul! The obedience, sufferings, and death of the God-man, 
    made over to the repenting, believing sinner, by an act of free and 
    sovereign grace, is the only plank on which the soul can safely rest--let it 
    attempt the passage across the cold river of death on any other, and it is 
    gone! On this it may boldly venture, and on this it shall be safely and 
    triumphantly carried into the quiet and peaceful haven of future and eternal 
    blessedness. We acknowledge the magnitude of this doctrine; yet it is not to 
    be rejected because of its greatness. It may be profound, almost too deeply 
    so for an angel&rsquo;s mind--the cherubim may veil their faces, overpowered with 
    its glory, while yet with eager longings they desire to look into it--still 
    may the weakest saint of God receive it, live upon it, walk in it. It is &quot;a 
    deep river, through which an elephant might swim, and which a lamb may 
    ford.&quot;<br>
    <br>
    <br>
