    JULY 10.<br>
    <br>
    &quot;Wherefore he is able also to save to the uttermost, those who come unto God 
    by him.&quot; Hebrews 7:25<br>
    <br>
    What a witness is this to the power and readiness of Christ to save! And 
    this is the testimony of the Holy Spirit to the blessed Son of God. But He 
    does more than this. He brings home the record with power to the soul. He 
    writes the testimony on the heart. He converts the believing soul itself 
    into a witness that &quot;Christ Jesus came into the world to save sinners.&quot;<br>
    <br>
    And what a gospel is this for a poor sinner! It speaks of pardon-of 
    acceptance-of peace-of full redemption here, and unspeakable glory 
    hereafter. It proclaims a Savior to the lost; a Redeemer to the captive; a 
    Surety to the insolvent; a Physician to the sick; a Friend to the needy; an 
    Advocate to the criminal;-all that a self-ruined, sin-accused, 
    law-condemned, justice-threatened, broken-hearted sinner needs, this 
    &quot;glorious gospel of the blessed God&quot; provides. It reveals to the self-ruined 
    sinner One in whom is his help, Hosea 13:9. To the sin-accused, One who can 
    take away all sin, 1 John 1:7. To the law-condemned, One who saves from all 
    condemnation, Romans 8:1. To the justice-threatened, One who is a 
    hiding-place from the wind, and a covert from the tempest, Isaiah 32:2. To 
    the broken-hearted, One who binds up and heals, Isaiah 61:1. That One 
    is-Jesus. O name ever dear, ever sweet, ever precious, ever fragrant, ever 
    healing to the &quot;poor in spirit&quot;!<br>
    <br>
    <br>
    What a witness, then, is this which the Eternal Spirit bears for Jesus! He 
    assures the believer that all he can possibly need is treasured up in 
    Christ-that he has no cross but Christ can bear it-no sorrow but Christ can 
    alleviate it-no corruption but Christ can subdue it-no guilt but Christ can 
    remove it-no sin but Christ can pardon it-no need but Christ can supply it. 
    Lift up your heads, you poor, you needy, you disconsolate! Lift up your 
    heads and rejoice that Christ is all to you-all you need in this valley of 
    tears-all you need in the deepest sorrow-all you need under the heaviest 
    affliction-all you need in sickness-all you will need in the hour of death 
    and in the day of judgment. <br>
    <br>
    Yes, and Christ is in all too. He is in all you salvation-He is in all your 
    mercies-He is in all your trials-He is in all your consolations, and in all 
    your afflictions. What more can you want? What more do you desire? A Father 
    who loves you as the apple of His eye-a full Savior to whom to go, moment by 
    moment-and a blessed indwelling, sanctifying, comforting Spirit, to reveal 
    all to you, and to give you Himself, as the &quot;pledge of your inheritance, 
    until the redemption of the purchased possession.&quot; &quot;Happy is that people 
    that is in such a case: yes, happy is that people whose God is the Lord.&quot;<br>
    <br>
    <br>
