    <p align="justify">FEBRUARY 25. </p>
    <p align="justify">&quot;Rest in the Lord, and wait patiently for Him.&quot; Psalm 
    37:7. </p>
    <p align="justify">It is just this simple, patient waiting upon God in all 
    our straits that certainly and effectually issues in our deliverance. In all 
    circumstances of faith's trial, of prayer's delay, of hope deferred, the 
    most proper and graceful posture of the soul- that which insures the largest 
    revenue of blessing to us and of glory to God- is a patient waiting on the 
    Lord. Although our impatience will not cause God to break His covenant, nor 
    violate His oath, yet a patient waiting will bring down larger and richer 
    blessings. The moral discipline of patience is most costly. It keeps the 
    soul humble, believing, prayerful. The mercy in which it results is all the 
    more prized and precious from the long season of hopeful expectation. It is 
    possible to receive a return too speedily. In our eagerness to grasp the 
    mercy with one hand, we may lose our hold on faith and prayer and God with 
    the other. A patient waiting the Lord's time and mode of appearing in our 
    behalf will tend to check all unworthy and unwise expedients and attempts at 
    self-rescue. An immediate deliverance may be purchased at a price too 
    costly. Its present taste may be sweet, but afterwards it may be bitter- God 
    embittering the blessing that was not sought with a single eye to His glory. 
    God's time, though it tarry, and God's deliverance, though delayed, when it 
    comes proves always to have been the best: &quot; My soul, wait only upon God, 
    for my expectation is from him.&quot; <br>
&nbsp; </p>
