    OCTOBER 19.<br>
    <br>
    &quot;The Lord God has given me the tongue of the learned, that I should know how 
    to speak a word in season to him that is weary.&quot; Isaiah 50:4<br>
    <br>
    THE Lord Jesus gives His people the tongue of the learned, the they may 
    sometimes speak a word in season to His weary ones. Have you not a word for 
    Christ? May you not go to that tried believer in sickness, in poverty, in 
    adversity, or in prison, and tell of the balm that has often healed your 
    spirit, and of the cordial that has often cheered your heart? &quot;A word spoken 
    in due season, how good is it!&quot; A text quoted, a sentiment repeated, an 
    observation made, a hint dropped, a kind caution suggested, a gentle rebuke 
    given, a tender admonition left-oh! the blessing that has flowed from it! It 
    was a word spoken in season! Say not with Moses, &quot;I am slow of speech, and 
    of a slow tongue;&quot; or with Jeremiah, &quot;Ah! Lord God! behold, I cannot speak; 
    for I am a child.&quot; Hear the answer of the Lord: &quot;Who has made man's mouth? 
    have not I, the Lord? Now therefore go: I will be with your mouth, and teach 
    you what you shall say.&quot; And oh! how frequently and effectually does the 
    Lord speak to His weary ones, even through the weary. All, perhaps, was 
    conflict within, and darkness without; but one word falling from the lips of 
    a man of God has been the voice of God to the soul. And what an honor 
    conferred, thus to be the channel conveying consolation from the loving 
    heart of the Father to the disconsolate heart of the child! to go and smooth 
    a ruffled pillow, lift the pressure from off a burdened spirit, and light up 
    the gloomy chamber of sorrow, of sickness, and of death, as with the first 
    dawnings of the coming glory! Go, Christian reader, and ask the Lord so to 
    clothe your tongue with holy, heavenly eloquence, that you may know how to 
    speak a word in season to him that is weary. Ah! it is impossible to speak 
    of the preciousness of Christ to another, and not, while we speak, feel Him 
    precious to our own souls. It is impossible to lead another to the cross, 
    and not find ourselves overshadowed by its glory. It is impossible to 
    establish another in the being, character, and truth of God, and not feel 
    our own minds fortified and confirmed. It is impossible to quote the 
    promises and unfold the consolations of the gospel to another, and not be 
    sensible of a tranquillizing and soothing influence stealing softly over our 
    own hearts. It is impossible to break the alabaster box, and not fill the 
    house with the odor of the ointment.<br>
    <br>
    In contending for the faith, remember that the Lord Jesus can give you the 
    tongue of the learned. Listen to His promises-&quot;I will give you a mouth and 
    wisdom, which all your adversaries shall not be able to gainsay nor resist.&quot; 
    Thus the most unlearned and the most weak may be so deeply taught, and be so 
    skillfully armed in Christ's school, as to be able valiantly to defend and 
    successfully to preach the truth, putting to &quot;silence the ignorance of 
    foolish men.&quot;<br>
    <br>
    <br>
