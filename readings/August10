    AUGUST 10.<br>
    <br>
    &quot;Every br>anch that bears fruit he prunes, that it may bring forth more
    fruit.&quot; John 15:2<br>
    <br>
    The Lord empties before He fills. He makes room for Himself, for His love,
    and for His grace. He dethrones the rival, casts down the idol, and seeks to
    occupy the temple, filled and radiant with His own ineffable glory. Thus
    does He br>ing the soul into great straits, lay it low, but to school and
    discipline it for richer mercies, higher service, and greater glory. Be sure
    of this, that, when the Lord is about to bless you with some great and
    peculiar blessing, He may prepare you for it by some great and peculiar
    trial. <br>
    <br>
    If He is about to advance you to some honor, He may first lay you low that
    He may exalt you. If He is about to place you in a sphere of great and
    distinguished usefulness, He may first place you in His school of adversity,
    that you may know how to teach others. If He is about to br>ing forth your
    righteousness as the noon-day, He may cause it to pass under a cloud, that,
    emerging from its momentary obscuration, it may shine with richer and more
    enduring luster. Thus does He deal with all His people. Thus He dealt with
    Joseph. Intending to elevate him to great distinction and influence, He
    first casts him into a dungeon, and that, too, in the very land in which he
    was so soon to be the gaze and the astonishment of all men. Thus, too, He
    dealt with David, and Job, and Nebuchadnezzar; and thus did God deal with
    His own Son, whom He advanced to His own right hand from the lowest state of
    humiliation and suffering.<br>
    <br>
    Regard the present suffering as but preparatory to future glory. This will
    greatly mitigate the sorrow, reconcile the heart to the trial, and tend
    materially to secure the important end for which it was sent. The life of a
    believer is but a disciplining for heaven. All the covenant dealings of His
    God and Father are but to make him a partaker of His holiness here, and thus
    to fit him for a partaker of His glory hereafter. Here, he is but schooling
    for a high station in heaven. He is but preparing for a more holy, and, for
    anything we know, a more active and essential service in the upper world.
    And every infirmity overcome, every sin subdued, every weight laid aside,
    every step advanced in holiness, does but strengthen and mature the life of
    grace below, until it is fitted for, and terminates in, the life of glory
    above. <br>
    <br>
    Let the suffering believer, then, see that he emerges from every trial of
    the furnace with some dross consumed, some iniquity purged, and with a
    deeper impress of the blessed Spirit's seal of love, holiness, and adoption,
    on his heart. Let him see that he has made some advance towards the state of
    the glorified; that He is more perfected in love and sanctification- the two
    great elements of heaven; and that therefore he is fitting for the
    inheritance of the saints in light. Blessed and holy tendency of all the
    afflictive dispensations of a covenant God and Father towards a dear and
    covenant child!<br>
    <br>
    <br>
