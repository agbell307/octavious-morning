    <p align="justify">FEBRUARY 17. </p>
    <p align="justify">&quot;Precious in the sight of the Lord is the death of his 
    saints.&quot; Psalm 116:15. </p>
    <p align="justify">It is solemnly true that there is a &quot;time to die.&quot; Ah! 
    affecting thought- a &quot;time to die!&quot; A time when this mortal conflict will be 
    over- when this heart will cease to feel, alike insensible to joy or sorrow- 
    when this head will ache and these eyes will weep no more- best and holiest 
    of all- a time &quot;when this corruptible shall put on incorruption, and this 
    mortal shall put on immortality,&quot; and we shall &quot;see Christ as He is, and be 
    like Him.&quot; If this be so, then, O Christian, why this anxious, trembling 
    fear? Your time of death, with all its attendant circumstances, is in the 
    Lord's hand. All is appointed and arranged by Him who loves you, and who 
    redeemed you- infinite goodness, wisdom, and faithfulness consulting your 
    highest happiness in each circumstance of your departure. The final sickness 
    cannot come, the &quot;last enemy&quot; cannot strike, until He bids it. All is in His 
    hand. Then calmly, confidingly, leave life's closing scene with Him. You 
    cannot die away from Jesus. Whether your spirit wings its flight at home or 
    abroad, amid strangers or friends, by a lingering process or by a sudden 
    stroke, in brightness or in gloom, Jesus will be with you; and, upheld by 
    His grace, and cheered with His presence, you shall triumphantly exclaim, 
    &quot;Though I walk through the valley of the shadow of death, I will fear no 
    evil; for you are with me: your rod and your staff, they comfort me,&quot; 
    bearing your dying testimony to the faithfulness of God, and the 
    preciousness of His promises. My time to die is in Your hand, O Lord, and 
    there I calmly leave it. <br>
&nbsp; </p>
