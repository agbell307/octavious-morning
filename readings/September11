    SEPTEMBER 12.<br>
    <br>
    &quot;And if children, then heirs; heirs of God.&quot; Romans 7:17<br>
    <br>
    NOT only are they begotten by God as His children, and by a sovereign act of 
    His most free mercy have become the heirs of an inheritance; but, 
    subjectively, they are made the heirs of Himself. &quot;Heirs of God.&quot; Not only 
    are all things in the covenant theirs, but the God of the covenant is 
    theirs. This is their greatest mercy. &quot;I am your part and your inheritance&quot; 
    are His words, addressed to all His spiritual Levites. Not only are they put 
    in possession of all that God has-a boundless wealth-but they are in present 
    possession of all that God is-an infinite portion. And what an immense truth 
    is this, &quot;I will be their God, and they shall be my people&quot;! Take out this 
    truth from the covenant of grace, were it possible, and what remains? It is 
    the chief wealth and the great glory of that covenant, that God is our God. 
    This it is that gives substance to its blessings, and security to its 
    foundation. So long as faith can retain its hold upon the God of the 
    covenant, as our God, it can repose with perfect security in expectation of 
    the full bestowment of all the rest. Here lies our vast, infinite, and 
    incomputable wealth. What constitutes the abject poverty of an ungodly man? 
    His being without God in the world. Be you, my reader, rich or poor, high or 
    low in this world, without God, you are undone to all eternity. It is but of 
    trivial moment whether you pass in rags and lowliness, or move in ermine and 
    pomp, to the torments of the lost; those torments will be your changeless 
    inheritance, living and dying without God, and without Christ, and without 
    hope. But contrast this with the state of the poorest child of God. The 
    universe is not only his-&quot;for all things are yours&quot;-but the God of the 
    universe is his: &quot;The Lord is my portion, says my soul, therefore will I 
    hope in Him.&quot; We have a deathless interest in every perfection of the Divine 
    nature. Is it Wisdom? it counsels us. Is it Power? it shields us. Is it 
    Love? it soothes us. Is it Mercy? it upholds us. Is it truth? it cleaves to 
    us. &quot;As the mountains are round about Jerusalem, so the Lord is round about 
    His people, from henceforth, even for evermore.&quot; What more can we ask than 
    this? If God be ours, we possess the substance and the security of every 
    other blessing. He would bring us to an absolute trust in an absolute God. 
    Winning us to an entire relinquishment of all expectation from any other 
    source, He would allure us to His feet with the language of the Church 
    breathing from our lips-&quot;Behold, we come unto You, for You are the Lord our 
    God. Truly in vain is salvation hoped for from the hills, and from the 
    multitude of mountains: truly in the Lord our God is the salvation of 
    Israel.&quot; It is in the heart of our God to give us the chief and the best. 
    Had there been a greater, a better, a sweeter, and a more satisfying portion 
    than Himself, then that portion had been ours. But since there is not, nor 
    can be, a greater than He, the love, the everlasting, changeless love that 
    He bears to us constrains Him to give Himself as our God, our portion, our 
    all. And have we not experienced Him to be God all-sufficient? Have we ever 
    found a want or a lack in Him? May He not justly challenge us, and ask, 
    &quot;Have I been a wilderness unto Israel? a land of darkness?&quot; Oh no! God is 
    all-sufficient, and no arid wilderness, no dreary land, have we experienced 
    Him to be. There is in Him an all-sufficiency of love to comfort us; an 
    all-sufficiency of strength to uphold us; an all-sufficiency of power to 
    protect us; and all-sufficiency of good to satisfy us; an all-sufficiency of 
    wisdom to guide us; an all-sufficiency of glory to reward us; and an 
    all-sufficiency of bliss to make us happy here, and happy to all eternity. 
    Such is the inheritance to which, as children of God, we are the heirs.<br>
    <br>
    <br>
