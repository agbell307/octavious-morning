    <p align="justify">MAY 18. </p>
    <p align="justify">&quot;You shall love the Lord your God with all your heart, 
    and with all your soul, and with all your mind. This is the first and great 
    commandment.&quot; Matthew 22:37-38 </p>
    <p align="justify">Love to God is spoken of in His word as forming the 
    primary and grand requirement of the divine law. Now, it was both infinitely 
    wise and good in God thus to present Himself the proper and lawful object of 
    love. We say it was wise, because, had He placed the object of supreme 
    affection lower than Himself, it had been to have elevated an inferior 
    object above Himself. For whatever other object than God is loved with a 
    sole and supreme affection, it is a deifying of that object, so that it, as 
    God, sits in the temple of God, showing itself that it is God. It was good, 
    because a lesser object of affection could never have met the desires and 
    aspirations of an immortal mind. God has so constituted man, implanting in 
    him such a capacity for happiness, and such boundless and immortal desires 
    for its possession, as can find their full enjoyment only in infinity 
    itself. He never designed that the intelligent and immortal creature should 
    sip its bliss at a lower fountain than Himself. Then, it was infinitely wise 
    and good in God that He should have presented Himself as the sole object of 
    supreme love and worship to His intelligent creatures. His wisdom saw the 
    necessity of having one center of supreme and adoring affection, and one 
    object of supreme and spiritual worship, to angels and to men. His goodness 
    suggested that that center and that object should be Himself, the perfection 
    of infinite excellence, the fountain of infinite good. That, as from Him 
    went forth all the streams of life to all creatures, it was but reasonable 
    and just that to Him should return, and in Him should center, all the 
    streams of love and obedience of all intelligent and immortal creatures: 
    that, as He was the most intelligent, wise, glorious, and beneficent object 
    in the universe, it was fit that the first, strongest, and purest love of 
    the creature should soar towards and find its resting-place in Him. <br>
    Love to God, then, forms the grand requirement and fundamental precept of 
    the divine law. It is binding upon all intelligent beings. From it no 
    consideration can release the creature. No plea of inability, no claim of 
    inferior objects, no opposition of rival interest, can lessen the obligation 
    of every creature that has breath to &quot;love the Lord his God with all his 
    heart, and with all his soul, and with all his mind.&quot; It grows out of the 
    relation of the creature to God, as his Creator, moral Governor, and 
    Preserver; and as being in Himself the only object of infinite excellence, 
    wisdom, holiness, majesty, and grace. This obligation, too, to love God with 
    supreme affection is binding upon the creature, irrespective of any 
    advantage which may result to him from so loving God. It is most true that 
    God has benevolently connected supreme happiness with supreme love, and has 
    threatened supreme misery where supreme affection is withheld; yet, 
    independent of any blessing that may accrue to the creature from its love to 
    God, the infinite excellence of the Divine nature and the eternal relation 
    in which He stands to the intelligent universe, render it irreversibly 
    obligatory on every creature to love Him with a supreme, paramount, holy, 
    and unreserved affection. <br>
&nbsp; </p>
