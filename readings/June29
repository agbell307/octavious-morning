    <p align="justify">JUNE 29. </p>
    <p align="justify">&quot;For we know in part, and we prophesy in part.&quot; 1 
    Corinthians 13:9 </p>
    <p align="justify">With all our attainments, how little have we really 
    attained! With all our knowledge, how little do we actually know! How 
    superficially and imperfectly are we acquainted with truth; with Jesus who 
    is emphatically &quot;the Truth,&quot; with God whom the Truth reveals. &quot;We see 
    through a glass darkly,&quot;- all is yet but as a riddle, compared with what we 
    shall know when the shadows of ignorance have fled. There are, too, the 
    enshrouding shadows of God's dark and painful dispensations. Our dealings 
    are with a God of whom it is said, &quot;Clouds and darkness are round about 
    Him.&quot; Who often &quot;covers Himself as with a cloud,&quot; and to whom the midnight 
    traveler to the world of light has often occasion to address himself in the 
    language of the Church, &quot;You are a God that hides Yourself.&quot; Ah! beloved, 
    what clouds of dark providences may be gathering and thickening around your 
    present path! Through what a gloomy, stormy night of affliction faith may be 
    steering your tempest-tossed barque! That faith eyeing the promise, and not 
    the providence, the &quot;bright light that is in the cloud,&quot; and not the 
    lowering cloud itself- will steer that trembling vessel safely through the 
    surge. Remember that in the providences of God the believer is passive, but 
    with regard to the promises of God he is active. In the one case he is to 
    &quot;be still&quot; and know that God reigns, and that the &quot;Judge of all the earth 
    must do right.&quot; In the other, his faith, childlike, unquestioning, and 
    unwavering, is to take hold of what God says, and of what God is, believing 
    that what He has promised He is also able and willing to perform. This is to 
    be &quot;strong in faith, giving glory to God.&quot; <br>
&nbsp; </p>
