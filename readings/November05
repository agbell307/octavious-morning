    NOVEMBER 5.<br>
    <br>
    &quot;For Christ is not entered into the holy places made with hands, which are 
    the figures of the true; but into heaven itself, now to appear in the 
    presence of God for us.&quot; Hebrews 9:24<br>
    <br>
    IS it a privilege to be borne upon the affectionate and believing prayers of 
    a Christian friend? Ah, yes! precious channels of heavenly blessing are the 
    intercessions of the Lord&rsquo;s people on our behalf. But there is a Friend 
    still closer to the Fountain of mercy, still nearer and dearer to the 
    Father, than your dearest earthly friend; it is Jesus, &quot;who ever lives to 
    make intercession for them who come unto God by Him.&quot; Oh, how precious is 
    that declaration upon which in any assault, or trial, or perplexity, you may 
    calmly and confidently repose: &quot;I have prayed for you&quot;! Yes, when from 
    confusion of thought, or pain of body, or burning fever, you can not pray 
    for yourself, and no friend is near to be your mouth to God, then there is 
    one, the Friend of friends, the ever-skillful Advocate and never-weary 
    Intercessor--no invocating saint, nor interceding angel--but the Son of God 
    himself, who appears in the presence of God moment by moment for you. Oh, 
    keep, then, the eye of your faith immovably fixed upon Christ&rsquo;s 
    intercession; He intercedes for weak faith, for tried faith, for tempted 
    faith--yes, for him who thinks he has no faith. There is not a believer who 
    is not borne upon His heart, and whose prayers and needs are not presented 
    in His ceaseless intercession. When you deem yourself neglected and 
    forgotten, a praying Savior in heaven is thinking of you. When you are tried 
    and cast down, tempted and stumble, the interceding High Priest at that 
    moment enters within the holiest, to ask on your behalf strength, 
    consolation, and upholding grace. And when sin has wounded, when guilt 
    distresses, and unbelief beclouds, who is it that stands in the breach, that 
    makes intercession, that removes the darkness, and brings back the smile of 
    a forgiving Father?--the Lord Jesus, the interceding Savior. Oh, look up, 
    tried and assaulted believer! you have a Friend at court, an Advocate in the 
    chancery of heaven, an Intercessor curtained within the holiest of holies, 
    transacting all your concerns, and through whom you may have access to God 
    with boldness.<br>
    <br>
    <br>
