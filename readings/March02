    <p align="justify">MARCH 2. </p>
    <p align="justify">&quot;Open you mine eyes, that I may behold wondrous things 
    out of your law.&quot; Psalm 119:18 </p>
    <p align="justify">To the question often earnestly propounded- &quot;What is the 
    best method of reading, so as to understand the Scriptures?&quot; I would reply- 
    Read them with the one desire and end of learning more of Christ, and with 
    earnest prayer for the teaching of the Spirit, that Christ may be unfolded 
    in the Word. With this simple method persevered in, you shall not fail to 
    comprehend the mind of the Holy Spirit, in portions which previously may 
    have been unintelligible and obscure. Restrict not yourself to fixed rules, 
    or to human helps. Rely less upon dictionaries, and maps, and annotations. 
    With singleness of aim, with a specific object of research, and with fervent 
    prayer for the Holy Spirit's teaching, &quot;you need not that any man teach 
    you;&quot; but collating Scripture with Scripture, &quot;comparing spiritual things 
    with spiritual,&quot; you may fearlessly enter upon the investigation of the 
    greatest mysteries contained in the sacred volume, assured that the Savior, 
    for whose glories and riches you search, will reveal Himself to your eye, 
    &quot;full of grace and truth.&quot; Precious Bible! so full of a precious Jesus! How 
    do all its clouds and darkness melt into light and beauty, as He, the Sun of 
    righteousness, rises in noontide glory upon its page! Search it, my reader, 
    with a view of seeing and knowing more of your Redeemer, compared with whom 
    nothing else is worth knowing or making known. Love your Bible, because it 
    testifies of Jesus; because it unfolds a great Savior, an almighty Redeemer; 
    because it reveals the glory of a sin-pardoning God, in the person of Jesus 
    Christ. Aim to unravel Jesus in the types, to grasp Him amid the shadows, to 
    trace Him through the predictions of the prophet, the records of the 
    evangelist, and the letters of the apostles. All speak of, and all lead to, 
    Jesus. &quot;They are they which testify of me.&quot; <br>
&nbsp; </p>
