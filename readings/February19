    <p align="justify">FEBRUARY 19. </p>
    <p align="justify">&quot;You know the grace of our Lord Jesus Christ, that, 
    though He was rich, yet for your sakes He became poor, that you through His 
    poverty might be rich.&quot; 2 Corinthians 8:9. </p>
    <p align="justify">How little do we associate our most costly mercies, and 
    even those which we are accustomed to esteem of a more ordinary character 
    (although every mercy is infinitely great), with the abasement of our Lord! 
    How seldom do we trace our happy moments, and hallowed joys, and high 
    delights, and sacred scenes, and precious privileges, to this dark part of 
    His eventful history! And yet all flow to us through this very channel, and, 
    but for this, would never have been ours. When the ocean of His goodness 
    rolls in upon me, wave on wave- when I feel the cheering warmth of creature 
    smiles beaming sweetly and fondly- when I review, one by one, my personal, 
    domestic, and relative mercies- when even the cup of cold water, presented 
    by the hand of Christian kindness, moistens my lips, what is the thought 
    that forces itself upon my mind? &quot;All this springs from the deepest 
    humiliation of my adorable Christ!&quot; <br>
    And when I ascend into the higher region of grace, and survey the blessings 
    so richly and so freely bestowed- a rebel subdued- a criminal pardoned- a 
    child adopted- a royal priest anointed- union with Christ- covenant 
    relationship with God- access within the Holy of Holies- conformity to the 
    Divine image- still more deeply am I overwhelmed with the thought, &quot;all this 
    proceeds from the infinite abasement of the incarnate God!&quot; <br>
    And when yet higher still I ascend, and, passing from grace to glory, 
    contemplate the heaven of bliss that awaits me- in one moment absent from a 
    body of sin, and present with the Lord- away from a world, beautiful though 
    it is, because God has made it, yet the throne of Satan, the empire of sin, 
    the scene of sorrow, pollution, suffering, and death; and eternally shut in 
    with God, where all is joy, and all is holiness- made perfectly holy, and, 
    consequently, perfectly happy, to sin no more, to sorrow no more, to weep no 
    more, to wander no more, to fall no more- oh, how full of glory then becomes 
    the humiliation of my incarnate Lord! Beloved, when God exalts you, remember 
    it is because your Savior was abased. When your cup is sweet, remember it is 
    because His cup was bitter. When you press your mercy fondly and closely to 
    your heart, remember it is because He pressed His heart to the spear. And 
    when your eye of faith and hope looks forward to the coming glory, oh, do 
    not forget that, because He endured your hell, you shall enjoy His heaven!
    <br>
&nbsp; </p>
