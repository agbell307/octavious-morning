    JULY 22.<br>
    <br>
    &quot;And the Holy Spirit helps us in our distress. For we don't even know what 
    we should pray for, nor how we should pray. But the Holy Spirit prays for us 
    with groanings that cannot be expressed in words.&quot; Romans 8:26<br>
    <br>
    The Holy Spirit is here represented in the character of a pleader or 
    advocate for the saints. To form a vivid conception of this truth, we have 
    but to imagine an anxious and embarrassed client prosecuting some important 
    suit, or, perchance, battling for his life in a court of justice. At his 
    side stands his counselor, thoroughly acquainted with the nature of his 
    case, and deeply versed in the bearings of the law. He is there to instruct 
    his client how to shape his course, with what arguments to support, with 
    what pleas to urge, with what words to clothe his suit. Such is the advocacy 
    and such the aid of the Spirit in the matter of prayer. We stand in the 
    presence of the Lord-it may be to deprecate a deserved punishment, or to 
    plead for a needed blessing. <br>
    <br>
    &quot;We don't even know what we should pray for, nor how we should pray.&quot; How 
    shall we order our cause before the Great Judge? With what feelings, with 
    what language, with what arguments shall we unburden our heart, unveil our 
    sorrow, confess our sin, and make known our request? How shall I overcome 
    the remembrance of past ingratitude, and the conviction of present guilt, 
    and the pressure of deep need, and the overwhelming sense of the Divine 
    Majesty? How shall I wake the heart to feeling; rouse the dull, sluggish 
    emotions of the soul; recall the truant affections; and concentrate the mind 
    upon the holy and solemn engagement? But our counselor is there! &quot;The Holy 
    Spirit prays for us.&quot; And how does He this? <br>
    <br>
    He indites the prayer. Do not think that that spiritual petition, which 
    breathed from your lips and rose as an incense-cloud before the mercy-seat, 
    was other than the inditing of the Holy Spirit. He inspired that prayer, He 
    created those desires, and He awoke those groanings. The form of your 
    petition may have been ungraceful, your language simple, your sentences 
    broken, your accents tremulous, yet there was an eloquence and a power in 
    that prayer which reached the heart and moved the arm of God. It overcame 
    the Angel of the Covenant. And whose eloquence and whose power was it?-the 
    interceding Spirit's. <br>
    He also teaches us what to pray for. Many and urgent as our needs are, we 
    only accurately know them as the Spirit makes them known. Alas! what 
    profound ignorance of ourselves must we cherish, when we know not what we 
    should ask God for as we ought! But the Spirit reveals our deep necessity, 
    convinces us of our emptiness, poverty, and need, and teaches us what 
    blessings to ask, what evils to deprecate, what mercies to implore. <br>
    <br>
    He sympathizes, too, with our infirmity in prayer, by portraying to our view 
    the parental character of God. Sealing on our hearts a sense of adoption, he 
    emboldens us to approach God with filial love and child-like confidence. He 
    leads us to God as a Father. Nor must we overlook the skill with which the 
    Spirit enables us to urge in our approaches to God the sinner's great 
    plea-the atoning blood of Jesus. This is no small part of the Divine aid we 
    receive in our infirmity. Satan, the accuser of the saints, even follows the 
    believer to the throne of grace to confront and confound him there. When 
    Joshua stood before the Angel of the Lord, Satan stood at his right hand to 
    resist him. But the Spirit, too, is there! He is there in the character, and 
    to discharge the office, of the praying soul's Intercessor. He instructs the 
    accused suppliant what arguments to use, what pleas to urge, and how to 
    resist the devil. He strengthens the visual organ of the soul, so that it 
    clearly discerns the blood upon the mercy-seat within the veil, on which it 
    fixes the eye in simple faith. Oh, it is the delight of the Spirit to take 
    of the things of Jesus-His love, His work, His sympathy, His grace, His 
    power-and show them to the soul prostrate in prayer before the throne of 
    grace.<br>
    <br>
    <br>
