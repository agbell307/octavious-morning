    <p align="justify">MAY 22. </p>
    <p align="justify">&quot;Bear you one another's burdens, and so fulfil the law of 
    Christ.&quot; Galatians 6:2 </p>
    <p align="justify">Thank God for an errand to Him. It may be you have felt 
    no heart to pray for yourself- you have been sensible of no peculiar 
    drawings to the throne for your own soul, but you halt gone in behalf of 
    another; the burden, the trial, the affliction, or the immediate need of 
    some member of God's family has pressed upon you, and you have taken his 
    case to the Lord: you have borne him in your arms to the throne of grace, 
    and, while interceding for your brother, the Lord has met you, and blessed 
    your own soul. Perhaps you halt gone and prayed for the Church, for the 
    peace of Jerusalem, for the prosperity of Zion, that the Lord would build up 
    her waste places, and make her a joy and a praise in the whole earth- 
    perhaps it has been to pray for your minister, that the Lord would teach him 
    more deeply and experimentally, and anoint him more plenteously with the 
    rich anointing and unction of the Holy Spirit- perhaps it has been to pray 
    for Christian missions, and for laborious and self-denying missionaries, 
    that the Lord would make them eminently successful in diffusing the 
    knowledge of a precious Savior, and in calling in His people: and thus, 
    while for others you have been besieging the throne of grace, and pouring 
    out your heart before the Lord, the Lord Himself has drawn near to your own 
    soul, and you have been made to experience the blessing that is ever the 
    attendant and the reward of intercessory prayer. Then let every event, every 
    circumstance, every providence be a voice urging you to prayer. If you have 
    no needs, others have- take them to the Lord. If you are borne down by no 
    cross, smitten by no affliction, or suffering from no need, others are- for 
    them go and plead with your heavenly Father, and the petitions you send up 
    to the mercy-seat on their behalf may return into your own bosom freighted 
    with rich covenant blessings. The falls, the weaknesses, the declensions of 
    others make them grounds for prayer. Thus, and thus only, can you expect to 
    grow in grace, and grace to grow in you. <br>
&nbsp; </p>
