    OCTOBER 28.<br>
    <br>
    &quot;Lord, you know all things; you know that I love you.&quot; John 21:17<br>
    <br>
    DEAR reader, this is His own solemn declaration of Himself-&quot;I, the Lord, 
    search the heart.&quot; Can you open all your heart to Him? Can you admit Him 
    within its most secret places? are you willing to have no concealments? Are 
    you willing that He should search and prove it? Oh, be honest with God!-keep 
    nothing back-tell Him all that you detect within you. He loves the full, 
    honest disclosure: He delights in this confiding surrender of the whole 
    heart. Are you honest in your desires that He might sanctify your heart, and 
    subdue all its iniquity?-then confess all to Him-tell Him all. You would not 
    conceal from your physician a single symptom of your disease-you would not 
    hide any part of the wound; but you would, if anxious for a complete cure, 
    disclose to him all. Be you as honest with the Great Physician-the Physician 
    of your soul. It is true, He knows your case; it is true, He anticipates 
    every want; yet He will have, and delights in having, His child approach Him 
    with a full and honest disclosure. Let David's example encourage you: &quot;I 
    acknowledged my sin unto You, and mine iniquity have I not hid; I said, I 
    will confess my transgressions unto the Lord; and You forgave the iniquity 
    of my sin.&quot; And while the heart is thus pouring itself out in a full and 
    minute confession, let the eye of faith be fixed on Christ. It is only in 
    this posture that the soul shall be kept from despondency. Faith must rest 
    itself upon the atoning blood. And oh, in this posture, fully and freely, 
    beloved reader, may you pour out your heart to God! Disclosures you dare not 
    make to your tenderest friend, you may make to Him: sins you would not 
    confess, corruption your would not acknowledge as existing within you, you 
    are privileged thus, &quot;looking unto Jesus,&quot; to pour into the ear of your 
    Father and God. And oh, how the heart will become unburdened, and the 
    conscience purified, and peace and joy flow into the soul, by this opening 
    of the heart to God! Try it, dear reader: let no consciousness of guilt keep 
    you back; let no unbelieving suggestion of Satan, that such confessions are 
    inappropriate for the ear of God, restrain you. Come at once-come now-to 
    your Father's feet, and bringing in your hands the precious blood of Christ 
    make a full and free disclosure. Thus from the attribute of Christ's 
    omniscience may a humble believer extract much consolation at all times 
    permitted to appeal to it, and say with Peter, &quot;Lord, You know all things, 
    You know that I love You.&quot;<br>
    <br>
    <br>
