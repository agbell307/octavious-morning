    <p align="justify">JUNE 14. </p>
    <p align="justify">&quot;Though I walk in the midst of trouble, you will revive 
    me.&quot; Psalm 138:7 </p>
    <p align="justify">Contemplate the Psalmist's circumstances &quot;Walking in the 
    midst of trouble.&quot; It was no new and untrodden path along which he was 
    pursuing his way to God. The foot-print, sometimes stained with blood, 
    always moistened with tears- of many a suffering pilgrim might be portrayed 
    in that way, from the time that Abel, the primeval martyr, laid the first 
    bleeding brow that ever reposed upon the bosom of Jesus. And yet how often 
    does trial overtake the believer, as &quot;though some strange thing had happened 
    to him&quot;! That at the peculiar nature of an affliction a Christian man should 
    be startled and alarmed, would create no surprise; but that he should be 
    startled at the trial itself, as if he alone- the only one of the family- 
    were exempted from the discipline of the covenant, and had no interest in 
    the Savior's declaration, &quot;In the world you shall have tribulation,&quot; might 
    well astonish us. <br>
    But David's experience is that of many of the spiritual seed of David. His 
    words seem to imply, continuous trial: &quot;I walk in the midst of trouble.&quot; 
    With how many travelers to the celestial city it is thus! They seem never to 
    be without trial. They know no cessation, they obtain no repose, they 
    experience no rest. The foam of one mountain billow has scarcely broken and 
    died upon the shore, before another follows in its wake- &quot;Deep calls unto 
    deep.&quot; Is it the trial of sickness? the darkened chamber, scarcely ever 
    illumined with one cheering ray of light, the bed of suffering, seldom 
    offering one moment's real repose, the couch of weariness, rarely left, are 
    vivid pictures of trial, drawn from real life, needing no coloring of the 
    fancy to heighten or exaggerate. Is it domestic trial? What scenes of 
    incessant chafings and anxieties, turmoils and sources of bitterness, do 
    some families present; trouble seems never to absent itself from the little 
    circle. Yes, it is through a series of trials that many of Christ's 
    followers are called to travel. The loss of earthly substance may be 
    followed by the decay of health, and this succeeded perhaps by that which, 
    of all afflictions, the most deeply pierces and lacerates the heart, and for 
    a season covers every scene with the dark pall of woe- the desolation of 
    death. Thus the believer ever journeys along a path paved with sorrow, and 
    hemmed in by trial. Well, be it so! We do not speak of it complainingly; God 
    forbid! We do not arraign the wisdom, nor doubt the mercy, nor impeach the 
    truth of Him who has drawn every line of that path, who has paved every step 
    of that way, and who knows its history from the end to the beginning. Why 
    should our heart fret against the Lord? Why should we weary at the way? It 
    is the ordained way- it is the right way- it is the Lord's way; and it is 
    the way to a city of habitation, where the soul and body- the companions of 
    the weary pilgrimage- will together sweetly and eternally rest. Then all 
    trouble ceases; then all conflict terminates. Emerging from the gloom and 
    labyrinth of the wilderness, the released spirit finds itself at home, the 
    inhabitant of a world of which it is said, &quot;God shall wipe away all tears 
    from their eyes; and there shall be no more death, neither sorrow, nor 
    crying, neither shall there be any more pain; for the former things are 
    passed away.&quot; <br>
&nbsp; </p>
