    <p align="justify">FEBRUARY 22. </p>
    <p align="justify">&quot;And this is the confidence that we have in Him, that, if 
    we ask anything according to His will, He hears us: and if we know that He 
    hear us, whatever we ask, we know that we have the petitions that we desired 
    of Him.&quot; 1 John 5:14, 15. </p>
    <p align="justify">When we draw near to God, and ask for more love, more 
    zeal, an increase of faith, a reviving of God's work within us, more 
    resemblance to Christ, the subjection of some enemy, the mortification of 
    some evil, the subduing of some iniquity, the pardon of some guilt, more of 
    the spirit of adoption, the sprinkling of the atoning blood, the sweet sense 
    of acceptance, we know and are assured that we ask for those things which 
    are according to the will of God, and which it is in the heart of God fully 
    and freely to bestow. There need be no backwardness here- there need be no 
    restraint here- there may be no misgiving here. The believer may, when 
    pleading for such blessings, spreading out such needs before the Lord, with 
    &quot;boldness enter into the holiest, by the blood of Jesus.&quot; He may draw near 
    to God, not standing afar off, but, in the spirit of a child, drawing near 
    to God, he may come with large requests, large desires, hopeful 
    expectations; he may open his mouth wide, because he asks those things which 
    it is glorifying to God to give, which glorify Him when given, and which we 
    know, from His own word, are according to His blessed will to bestow. Oh, 
    the unspeakable encouragement of going to God with a request which we feel 
    assured it is in His heart and according to His will freely to grant! <br>
&nbsp; </p>
