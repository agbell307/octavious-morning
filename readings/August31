    AUGUST 31.<br>
    <br>
    &quot;Nevertheless, afterward it yields the peaceable fruit of righteousness unto
    those who are exercised thereby.&quot; Hebrews 12:11<br>
    <br>
    The very wisdom seen in this method of instruction� the sanctified
    discipline of the covenant, proves its divine origin. Had the believer been
    left to form his own school, adopt his own plan of instruction, choose his
    own discipline, and even select his own teacher, how different would it have
    been from God's plan! We would never have conceived the idea of such a mode
    of instruction, so unlikely, according to our poor wisdom, to secure the end
    in view. We would have thought that the smooth path, the sunny path, the
    joyous path, would the soonest conduct us into the glories of the kingdom of
    grace; would more fully develop the wisdom, the love, the tenderness, the
    sympathy of our blessed Lord, and tend more decidedly to our weanedness from
    the world, our crucifixion of sin, and our spiritual and unreserved
    devotedness to His service. But &quot;my thoughts are not your thoughts, neither
    are your ways my ways, says the Lord. For as the heavens are higher than the
    earth, so are my ways higher than your ways, and my thoughts than your
    thoughts.&quot;<br>
    <br>
    Nor is the believer fully convinced of the wisdom of God's method of
    procedure until he has been br>ought, in a measure, through the discipline;
    until the rod has been removed, the angry waves have subsided, and the
    tempest cloud has passed away. Then, reviewing the chastisement, minutely
    examining its nature and its causes; the steps that led to it; the chain of
    providences in which it formed a most important link; and most of all,
    surveying the rich covenant blessings it br>ought with it� the weanedness
    from the world, the gentleness, the meekness, the patience, the
    spirituality, the prayerfulness, the love, the joy; he is led to exclaim, &quot;I
    now see the infinite wisdom and tender mercy of my Father in this
    affliction. While in the furnace I saw it not; the rising of inbred
    corruption, unbelief, and hard thoughts of God darkened my view, veiled from
    the eye of my faith the reason of the discipline; but now I see why and
    wherefore my covenant God and Father has dealt with me thus: I see the
    wisdom and adore the love of His merciful procedure.&quot;<br>
    <br>
    Other discipline may mortify, but not humble the pride of the heart; it may
    wound, but not crucify it. Affliction, sanctified by the Spirit of God, lays
    the soul in the dust; gives it low thoughts of itself. Gifts, attainments,
    successful labors, the applause of men, all conspire the ruin of a child of
    God; and, but for the prompt and often severe discipline of an
    ever-watchful, ever-faithful God, would accomplish their end. But the
    affliction comes; the needed cross; the required medicine; and in this way
    are br>ought out &quot;the peaceable fruits of righteousness;&quot; the most beautiful
    and precious of which is a humble, lowly view of self.</p
