    NOVEMBER 13.<br>
    <br>
    &quot;Take my yoke upon you, and learn of me; for I am meek and lowly in heart: 
    and you shall find rest unto your souls. For my yoke is easy, and my burden 
    is light.&quot; Matthew 11:29, 30<br>
    <br>
    HOW shall we array, in their strongest light, before you, the motives which 
    urge the cultivation of this poverty of spirit? Is it not enough that this 
    is the spiritual state on which Jehovah looks with an eye of exclusive, 
    holy, and ineffable delight? &quot;To this man I will look.&quot; Splendid gifts, 
    brilliant attainments, costly sacrifices, are nothing to me. &quot;To this man 
    will I look, that is poor and of a contrite spirit, and that trembles at my 
    word.&quot; To this would we add, if you value your safe, happy, and holy walk--if 
    you prize the manifestations of God&rsquo;s presence--the &quot;kisses of His mouth, 
    whose love is better than wine&quot;-- the teaching, guiding, and comforting 
    influence of the Holy Spirit, seek it. If you would be a &quot;savor of Christ in 
    every place&quot;--if you would pray with more fervor, unction, and power--if you 
    would labor with more zeal, devotedness, and success, seek it. By all that 
    is dear, and precious, and holy, by your own happiness, by the honor of 
    Christ, by the glory of God, by the hope of heaven, seek to be found among 
    those who are &quot;poor and of a contrite spirit,&quot; who, with filial, holy love, 
    tremble at God&rsquo;s word, whom Jesus has pronounced blessed here, and meet for 
    glory hereafter. And though in approaching the Great High Priest, you have 
    no splendid and costly intellectual offerings to present, yet with the royal 
    penitent you can say, &quot;You desire not sacrifice, else would I give it: you 
    delight not in burned offering. The sacrifices of God are a broken spirit: a 
    broken and a contrite heart, O God, you will not despise.&quot; &quot;This, Lord, is 
    all that I have to bring You.&quot; Avoid a spurious humility. True humility 
    consists not in denying the work of the Holy Spirit in our hearts, in 
    under-rating the grace of God in our souls, in standing afar off from our 
    heavenly Father, and in walking at a distance from Christ, always doubting 
    the efficacy of His blood, the freeness of His salvation, the willingness of 
    His heart, and the greatness of His power to save. Oh no! this is not the 
    humility that God delights to look at, but is a false, a counterfeit 
    humility, obnoxious in His sight. But to &quot;draw near with a true heart, in 
    full assurance of faith,&quot; in lowly dependence upon His blood and 
    righteousness; to accept of salvation as the gift of His grace; to believe 
    the promise because He has spoken it; gratefully and humbly to acknowledge 
    our calling, our adoption, and our acceptance, and to live in the holy, 
    transforming influence of this exalted state, giving to a Triune God all the 
    praise and glory; this is the humility which is most pleasing to God, and is 
    the true product of the Holy Spirit.<br>
    <br>
    <br>
