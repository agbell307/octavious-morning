    NOVEMBER 20.<br>
    <br>
    &quot;God, who quickens the dead.&quot; Romans 4:17<br>
    <br>
    THE commencement of spiritual life is sudden. We are far from confining the 
    Spirit to a certain prescribed order in this or any other part of His work. 
    He is a Sovereign, and therefore works according to His own will. But there 
    are some methods He more frequently adopts than others. We would not say 
    that all conversion is a sudden work. There is a knowledge of sin, 
    conviction of its guilt, repentance before God on account of it; these are 
    frequently slow and gradual in their advance. But the first communication of 
    divine light and life to the soul is always sudden--sudden and instantaneous 
    as was the creation of natural light--&quot;God said, Let there be light, and 
    there was light.&quot; It was but a word, and in an instant chaos rolled away, 
    and every object and scene in nature was bathed in light and glory--sudden as 
    was the communication of life to Lazarus--&quot;Jesus cried with a loud voice, 
    Lazarus, come forth!&quot; it was but a word, and in an instant &quot;he that was dead 
    came forth, bound hand and foot with grave-clothes.&quot; So is it in the first 
    communication of divine light and life to the soul. The eternal Spirit says, 
    &quot;Let there be light,&quot; and in a moment there is light. He speaks again, &quot;Come 
    forth,&quot; and in a moment, in the twinkling of an eye, the dead are raised.<br>
    <br>
    Striking illustrations of the suddenness of the Spirit&rsquo;s operation are 
    afforded in the cases of Saul of Tarsus and of the thief upon the cross. How 
    sudden was the communication of light and life to their souls! It was no 
    long and previous process of spiritual illumination--it was the result of no 
    lengthened chain of reasoning--no labored argumentation. In a moment, and 
    under circumstances most unfavorable to the change, as we should 
    think--certainly, at a period when the rebellion of the heart rose the most 
    fiercely against God, &quot;a light from heaven, above the brightness of the 
    sun,&quot; poured its transforming radiance into the mind of the enraged 
    persecutor; and a voice, conveying life into the soul, reached the 
    conscience of the dying thief. Both were translated from darkness into 
    light, &quot;in a moment, in the twinkling of an eye.&quot; How many who read this 
    page may say, &quot;Thus it was with me!&quot; God the Eternal Spirit arrested me when 
    my heart&rsquo;s deep rebellion was most up in arms against Him. It was a sudden 
    and a short work, but it was mighty and effectual. It was unexpected and 
    rapid, but deep and thorough. In a moment the hidden evil was brought to 
    view--the deep and dark fountain broken up; all my iniquities passed before 
    me, and all my secret sins seemed placed in the light of God&rsquo;s countenance. 
    My soul sank down in deep mire--yes, hell opened its mouth to receive me.&quot;<br>
    <br>
    Overlook not this wise and gracious method of the blessed Spirit&rsquo;s operation 
    in regeneration. It is instantaneous. The means may have been simple; 
    perhaps it was the loss of a friend--an alarming illness--a word of reproof or 
    admonition dropped from a parent or a companion--the singing of a hymn--the 
    hearing of a sermon--or some text of Scripture winged with his power to the 
    conscience; in the twinkling of an eye, the soul, &quot;dead in trespasses and 
    sins,&quot; was &quot;quickened&quot; and translated into &quot;newness of life.&quot; Oh blessed 
    work of the blessed and Eternal Spirit! Oh mighty operation! Oh inscrutable 
    wisdom! What a change has now passed over the whole man! Overshadowed by the 
    Holy Spirit, that which is begotten in the soul is the divine life--a holy, 
    influential, never-dying principle. Truly he is a new creature, &quot;old things 
    are passed away; behold, all things are become new.&quot; For this change let it 
    not be supposed that there is, in the subject, any previous preparation. 
    There can be no preparation for light or life. What preparation was there is 
    chaos? What preparation was there in the cold clay limbs of Lazarus? What in 
    Paul? What in the dying thief? The work of regeneration is supremely the 
    work of the Spirit. The means may be employed, and are to be employed, in 
    accordance with the Divine purpose, yet are they not to be deified. They are 
    but means, &quot;profiting nothing&quot; without the power of God the Holy Spirit. 
    Regeneration is His work, and not man&rsquo;s.<br>
    <br>
    <br>
