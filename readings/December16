    DECEMBER 16.<br>
    <br>
    &quot;Knowing that a man is not justified by the works of the law, but by the 
    faith of Jesus Christ, even we have believed in Jesus Christ, that we might 
    be justified by the faith of Christ, and not by the works of the law: for by 
    the works of the law shall no flesh be justified.&quot; Galatians 2:16<br>
    <br>
    THE term is forensic--employed in judicial affairs, transacted in a court of 
    judicature. We find an illustration of this in God&rsquo;s word. &quot;If there be a 
    controversy between men, and they come into judgment, that the judge may 
    judge them, then they shall justify the righteous, and condemn the wicked.&quot; 
    It is clear from this passage that the word stands opposed to a state of 
    condemnation, and in this sense it is employed in the text under 
    consideration. To justify, in its proper and fullest sense, is to release 
    from all condemnation. Now, it is important that we do not mix up this 
    doctrine, and the Church of Rome has done, with other and kindred doctrines. 
    We must clearly distinguish it from that of sanctification. Closely 
    connected as they are, they yet entirely differ. The one is a change of 
    state, the other a change of condition. By the one we pass from guilt to 
    righteousness, by the other we pass from sin to holiness. In justification 
    we are brought near to God; in sanctification we are made like God. The one 
    places us before Him in a condition of non-condemnation; the other 
    transforms us into His image. Yet the Church of Rome blends the two states 
    together, and in her formularies teaches an imputed sanctification, just as 
    the Bible teaches an imputed justification. It is to be distinguished, too, 
    from pardon. Justification is a higher act. By the act of pardon we are 
    saved from hell; but by the decree of justification we are brought to 
    heaven. The one discharges the soul from punishment; the other places in its 
    hand a title-deed to glory.<br>
    <br>
    The Lord Jesus Christ is emphatically the justification of all the 
    predestined and called people of God. &quot;By Him all that believe are justified 
    from all things.&quot; The antecedent step was to place Himself in the exact 
    position of His Church. In order to do this, it was necessary that He should 
    be made under the law; for, as the Son of God, He was above the law, and 
    could not therefore be amenable to its precept. But when He became the Son 
    of man, it was as though the sovereign of a vast empire had relinquished his 
    regal character for the condition of the subject. He, who was superior to 
    all law, by His mysterious incarnation placed Himself under the law. He, who 
    was the King of Glory, became by His advent the meanest of subjects. What a 
    stoop was this! What a descending of the Son of God from the height of His 
    glory! The King of kings, the Lord of lords, consenting to be brought under 
    His own law, a subject to Himself, the Law-giver becoming the law-fulfiller. 
    Having thus humbled Himself, He was prepared, as the sacrificial Lamb, to 
    take up and bear away the sins of His people. The prophecy that predicted 
    that He should &quot;bear their iniquities,&quot; and that He should &quot;justify many,&quot; 
    received in Him its literal and fullest accomplishment. Thus upon Jesus were 
    laid all the iniquities, and with the iniquities the entire curse, and added 
    to the curse, the full penalty, belonging to the Church of God. This 
    personal and close contact with sin affected not His moral nature; for that 
    was essentially sinless, and could receive no possible taint from His 
    bearing our iniquity. He was accounted &quot;accursed,&quot; even as was Israel&rsquo;s 
    goat, when upon its head Aaron laid the sins of the people; but as that 
    imputation of sin could not render the animal to whom it was transferred 
    morally guilty, though by the law treated as such, so the bearing of sin by 
    Christ could not for a single instant compromise His personal sanctity. With 
    what distinctness has the Spirit revealed, and with what strictness has He 
    guarded, the perfect sinlessness of the atoning Savior! &quot;He has made Him to 
    be sin for us, who knew no sin, that we might be made the righteousness of 
    God in Him.&quot; Oh blessed declaration to those who not only see the sin that 
    dwells in them, but who trace the defilement of sin in their holiest things, 
    and who lean alone for pardon upon the sacrifice of the spotless Lamb of 
    God! To them, how encouraging and consolatory the assurance that there is a 
    sinless One who, coming between a holy God and their souls, is accepted in 
    their stead, and in whom they are looked upon as righteous! And this is 
    God&rsquo;s method of justification.<br>
    <br>
    <br>
