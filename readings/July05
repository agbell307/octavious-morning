    JULY 5.<br>
    <br>
    &quot;A sure foundation.&quot; Isaiah 28:16<br>
    <br>
    &quot;A sure foundation&quot; is the last quality of excellence specified of this 
    precious Stone. As if, in so momentous a matter as the salvation of the 
    soul, to remove all lingering doubt from the mind, to annihilate all 
    imaginary and shadowy conceptions of Jesusl; Jehovah, the great Builder of 
    the Church, declares the foundation thus laid to be a real and substantial 
    one. Confidently here may the weary rest, and the sinner build his hope of 
    heaven. All is sure. Sure that the word he credits is true-sure that the 
    invitation that calls him is sincere-sure that the welcome extended to him 
    is cordial. Sure, in coming to Jesus, of free forgiveness, of full 
    justification, of complete and eternal acceptance with a reconciled God. 
    Sure, that in renouncing all self-dependence, and building high his hope of 
    glory on this foundation, he &quot;shall not be ashamed nor confounded, world 
    without end.&quot; All, too, is sure to the believer in the covenant of grace, of 
    which Jesus is the Surety and Mediator. Every promise is sure-the full 
    supply of all our need-the daily efficacy of the atoning blood-the answer to 
    our prayers, though long delayed-the hope of being forever with Jesus-all, 
    all is certain and sure, because based on Jesus, and springing from the 
    heart of an unchangeable God, and confirmed by the oath of Him who has said, 
    &quot;Once have I sworn by my holiness that I will not lie unto David.&quot;<br>
    <br>
    Precious Jesus! we have been contemplating Your glory as through a glass 
    darkly. And yet we thank and adore You even for this glimpse. Dim and 
    imperfect though it is, it has endeared You-unutterably endeared You-to our 
    hearts. Oh! if this is Your glory beheld through a clouded medium, what will 
    it be seen face to face! Soon, soon shall we gaze upon it. Then, Oh glorious 
    King, we shall exclaim, &quot;It was a true report that I heard of your acts and 
    of your wisdom, and, behold, the half was not told me.&quot; Seeing that we look 
    for such things, grant us grace, that being &quot;diligent, we may be found of 
    You in peace, without spot, and blameless.&quot; Send to us what You will, 
    withhold from us what You will; only vouchsafe to us a &quot;part in the first 
    resurrection,&quot; and a seat at Your right hand when You come to Your kingdom. 
    Low at Your feet we fall! Here may Your Spirit reveal to us more of Your 
    glory! Oh, irradiate, sanctify, and cheer us with its beams! Behold, we 
    cling to You! You are our Emmanuel, or portion, and our all. In darkness we 
    repair to the fountain of Your light. In sorrow, we flee to the asylum of 
    Your bosom. Oppressed, we come to the shelter of Your cross. Oh, take our 
    hearts, and bind them closer and still closer to Yourself! Won by Your 
    beauty and drawn by Your love, let there be a renewed surrender of our whole 
    spirit, and soul, and body. Oh, claim a fresh possession of us. &quot;Your 
    statutes have been our songs in the house of our pilgrimage: You shall guide 
    us with Your counsel, and afterward receive us to glory.&quot; Then shall we 
    unite with the Hallelujah Chorus, and sing in strains of surpassing 
    sweetness, gratitude, and love. &quot;Thanks be unto God for His unspeakable 
    gift!&quot;<br>
    <br>
    <br>
