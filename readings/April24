    <p align="justify">APRIL 24. </p>
    <p align="justify">&quot;Precious faith.&quot; 2 Peter 1:1 </p>
    <p align="justify">Truly is faith the crowning grace of all, and a most 
    costly and precious fruit of the renewed mind. From it springs every other 
    grace of a gracious soul. It has been designated the 'queen' grace, because 
    a royal train ever attends it. Faith comes not alone, nor dwells alone, nor 
    works alone. Where faith in Jesus is, there also are love, joy, peace, 
    long-suffering, patience, godly sorrow, and every kindred perfection of the 
    Christian character, all blending in the sweetest harmony, all uniting to 
    celebrate the glory of God's grace, and to crown Jesus Lord of all. Is it, 
    then, surprising that this should be distinguished from all the others by 
    the term &quot;precious faith&quot;? No! that must needs be precious which unfolds the 
    preciousness of everything else. It makes the real gold more precious, and 
    it transmutes everything else into gold. It looks to a &quot;precious Christ&quot; It 
    leads to His &quot;precious blood.&quot; It relies upon the &quot;precious promises.&quot; And 
    its very trial, though it be by fire, is &quot;precious.&quot; It so changes the 
    nature of the painful, the humiliating, and the afflictive, as to turn a 
    Father's frown, rebuke, and correction, into some of the costliest mercies 
    of life. Precious grace, that bids me look upon God in Christ as reconciled; 
    and which, in the absence of all evidence of sight, invites me to rest upon 
    the veracity of God! which takes me in my deepest poverty to Jesus, my true 
    Joseph, having in His hands and at His disposal all the treasures of grace 
    and glory! These are some of the characteristics of this royal grace. &quot;Being 
    justified by faith, we have peace with God, through our Lord Jesus Christ.&quot; 
    By faith I can not only say that Jesus died for sinners, but that He died 
    for me. Faith makes the great atonement mine. Faith appropriates to itself 
    all that is in Christ. It lays its hand upon the covenant of grace, and 
    exclaims, &quot;All things are mine.&quot; Oh, to see one bowed to the dust under a 
    sense of sin, yet by faith traveling to the blood and righteousness of the 
    Lord Jesus for salvation, and finding it too- to mark the power of this 
    grace in sustaining the soul in deep waters, holding it up in perilous 
    paths- is a spectacle on which God Himself must look down with ineffable 
    delight. <br>
&nbsp; </p>
