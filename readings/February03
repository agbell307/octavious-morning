    <p align="justify">FEBRUARY 3. </p>
    <p align="justify">&quot;There remains, then, a rest for the people of God.&quot; 
    Hebrews 4:9. </p>
    <p align="justify">Not yet come to the heavenly rest, we still are 
    approaching it, and, oh ecstatic thought! we shall reach it at last. 
    Everything in our present course reminds us that we are nearing home, as the 
    seaweed washed from the rocks, and as the land-birds venturing from their 
    bowers and floating by the vessel, are indices to the voyager that he is 
    nearing his port. Are you bereaved? Weep not! earth has one tie the less, 
    and heaven has one tie more. Are you impoverished of earthly substance? 
    Grieve not! your imperishable treasure is in heaven. Are you sailing over 
    dark and stormy waters? Fear not! the rising flood but lifts your ark the 
    higher and nearer the mount of perfect safety and endless rest. Are you 
    battling with disease, conscious that life is ebbing and eternity is 
    nearing? Tremble not! there is light and music in your lone and shaded 
    chamber- the dawn and the chimings of your heavenly home. &quot;I am going home! 
    Transporting thought!- True, I leave an earthly one, all so sweet and 
    attractive, but I exchange it for a heavenly one infinitely brighter, more 
    sacred and precious. I am going to Jesus- to the Church Triumphant- to 
    Apostles, Prophets, and Martyrs- to the dear ones who line the shore on the 
    other side, prepared to welcome me there. Death, from which I have so often 
    recoiled, is but the triumphal arch- oh, how bright a risen Christ has made 
    it! -through which I pass into 'my Father's house.'&quot; <br>
&nbsp; </p>
