    AUGUST 18.<br>
    <br>
    &quot;For we wrestle not against flesh and blood, but against principalities,
    against powers, against the rulers of the darkness of this world, against
    spiritual wickedness in high places.&quot; Ephesians 6:12<br>
    <br>
    Let us inquire what is that which Satan desires to assault? It is the work
    of God in the soul. Against his own kingdom not a weapon is raised. It is
    his aim and his policy to keep all there undisturbed and peaceful. But
    against the work of the Holy Spirit in the renewed mind, his artillery is
    br>ought to bear; not a part of this work escapes him. Every grace comes in
    for its share of malignant attack; but especially the grace of faith. When,
    for example, a repentant and believing soul approaches Christ with lowliness
    and hesitancy, and with the tremulous hand of faith attempts to touch the
    border of His garment, or with a tearful eye looks up to His cross, then
    comes the assault upon faith in the form of a suggestive doubt of Christ's
    power and willingness to save. &quot;Is Jesus able to save me? Has He power to
    rescue my soul from hell? Can He blot out my transgressions, and redeem my
    life from destruction? Will He receive a sinner, so vile, so unworthy, so
    poor as I? Has He compassion, has He love, has He mercy sufficient to meet
    my case?&quot; <br>
    <br>
    In this way Satan assails the earliest and the feeblest exercises of faith
    in the soul. Does this page address itself to any such? It is Satan's great
    effort to keep you from Jesus. By holding up to your view a false picture of
    His character, from which everything loving, winning, inviting, and
    attractive is excluded, by suggesting wrong views of His work, in which
    everything gloomy, contracted, and repulsive is foisted upon the mind; by
    assailing the atonement, questioning the compassion, and limiting the grace
    of Christ, he would persuade you that in that heart which bled on Calvary
    there is no room for you, and that upon that work which received the
    Father's seal there is not br>eadth sufficient for you to stand. All his
    endeavors are directed, and all his assaults are shaped, with a view to keep
    your soul back from Christ. It is thus he seeks to vent his wrath upon the
    Savior, and his malignity upon you.<br>
    <br>
    Nor does he less assail the more matured faith of the believer. Not
    infrequently the sharpest attacks and the fiercest onsets are made, and made
    successfully, upon the strongest believers. Seizing upon powerful
    corruptions, taking advantage of dark providences, and sometimes of br>ight
    ones, and never allowing any position of influence, any usefulness, gift, or
    grace, that would give force, success, and br>illiance to his exploit, to
    escape his notice, he is perpetually on the alert to sift and winnow God's
    precious wheat.<br>
    <br>
    His implacable hatred of God, the deep revenge he cherishes against Jesus,
    his malignant opposition to the Holy Spirit, fit him for any dark design and
    work implicating the holiness and happiness of the believer. Therefore we
    find that the histories of the most eminent saints of God, as written by the
    faithful pen of the Holy Spirit, are histories of the severest temptations
    of faith, in the most of which there was a temporary triumph of the enemy;
    the giant oak bending before the storm. And even in instances where there
    was no defeat of faith, there yet was the sharp trial of faith. <br>
    <br>
    The case of Joseph, and that of his illustrious antitype, the Lord Jesus,
    present examples of this. Fearful was the assault upon the faith of both,
    sharp the conflict through which both passed, yet both left the battlefield
    victorious. But still faith was not the less really or severely sifted.<br>
    <br>
    <br>
