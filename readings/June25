    <p align="justify">JUNE 25. </p>
    <p align="justify">&quot;Walk in love, as Christ also has loved us, and has given 
    himself for us an offering and a sacrifice to God for a sweet-smelling 
    savor. Ephesians 5:2 </p>
    <p align="justify">It was an entire sacrifice. It was Himself He offered up. 
    More He could not give; less would not have sufficed. He gave Himself- all 
    that He possessed in heaven, and all that belonged to Him on earth, He gave 
    in behalf of His people. His life of obedience, His death of suffering, He 
    gave as &quot;an offering and is sacrifice to God.&quot; It was an entire surrender.
    <br>
    It was a voluntary offering. &quot;He gave Himself.&quot; It was not by compulsion or 
    by constraint that He surrendered Himself into the hands of Divine justice- 
    He went not as a reluctant victim to the altar- they dragged Him not to the 
    cross. He went voluntarily. It is true that there existed a solemn 
    necessity, why Jesus should die in behalf of His people. It grew out of His 
    covenant engagement with the Father. Into that engagement He voluntarily 
    entered: His own ineffable love constrained Him: But after the compact had 
    been made, the covenant of redemption ratified, and the bond given to 
    justice, there was a necessity resting upon Jesus why He should finish the 
    work. His word, His honor, His truth, His glory, all were pledged to the 
    entire fulfilment of His suretyship. He had freely given Himself into the 
    power of justice; He was therefore, on His taking upon Him the form of a 
    servant, under obligations to satisfy all its claims; He was legally bound 
    to obey all its commands. And yet it was a voluntary surrender of Himself as 
    a sacrifice for His people. It was a willing offering. If there was a 
    necessity, and we have shown that there was, it grew out of His own 
    voluntary love to His Church. It was, so to speak, a voluntary necessity. 
    See how this blessed view of the death of Jesus is sustained by the Divine 
    word. &quot;He was oppressed, and He was afflicted, yet He opened not His mouth: 
    He is brought as a lamb to the slaughter, and as a sheep before her shearers 
    is dumb, so He opens not His mouth.&quot; His own declaration confirms the truth. 
    &quot;Therefore does my Father love me, because I lay down my life, that I might 
    take it again. No man takes it the following is from me, but I lay it down 
    of myself. I have power to lay it down, and I have power to take it again.&quot;
    <br>
&nbsp; </p>
