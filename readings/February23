    <p align="justify">FEBRUARY 23. </p>
    <p align="justify">&quot;For this thing I besought the Lord thrice, that it might 
    depart from me.&quot; 2 Corinthians 12:8. </p>
    <p align="justify">When Paul prayed for the removal of the thorn in the 
    flesh, he asked that of God which betrayed a lack of judgment in his 
    estimate of the thing which he petitioned for. Who would have suspected this 
    in the apostle of the Gentiles? But the Lord knew best what was for the good 
    of His dear servant. He saw that, on account of the peculiar revelations 
    that were given him in his visit to glory, the discipline of the covenant 
    was needed to keep him low in the dust. And, when His child petitioned 
    thrice for the removal of the thorn in the flesh, he for a moment 
    overlooked, in the painful nature of the discipline, its needed influence to 
    keep him &quot;walking humbly with God.&quot; So that we see even an inspired apostle 
    may ask those things of God, which He may see fit to refuse. We may 
    frequently expect some trial, something to keep us low before God, after a 
    season of peculiar nearness to Him, a manifestation of His loving-kindness 
    to our souls. There is a proneness to rest in self-complacency after close 
    communion with God, that the gentle hand of our Father is needed to screen 
    us from ourselves. It was so with Paul- why may it not be with us? In 
    withholding, however, the thing we ask of Him, we may be assured of this, 
    that He will grant us a perfect equivalent. The Lord saw fit to deny the 
    request of the apostle; but He granted him an equivalent- yes, more than an 
    equivalent, to that which He denied him- He gave him His all-supporting 
    grace. &quot;My grace is suffcient for you.&quot; Beloved reader, have you long asked 
    for the removal of some secret, heavy, painful cross? Perhaps you are yet 
    urging your request, and yet the Lord seems not to answer you. And why? 
    Because the request may not be in itself wise. Were He now to remove that 
    cross, He may, in taking away the cross, close up a channel of mercy which 
    you would never cease to regret. Oh, what secret and immense blessing may 
    that painful cross be the means of conveying into your soul! <br>
&nbsp; </p>
