    DECEMBER 22.<br>
    <br>
    &quot;It is a faithful saying: For if we be dead with him, we shall also live 
    with him: if we suffer, we shall also reign with him.&quot; 2 Timothy 2:11, 12<br>
    <br>
    BEHOLD, then, your exalted privilege, you suffering sons of God! See how the 
    glory beams around you, you humble and afflicted ones! You are one with the 
    Prince of sufferers, and the Prince of sufferers is one with you! Oh! to be 
    one with Christ--what tongue can speak, what pen can describe the sweetness 
    of the blessing, and the greatness of the grace? To sink with Him in His 
    humiliation here is to rise with Him in His exaltation hereafter. To share 
    with Him in His abasement on earth is to blend with Him in His glory in 
    heaven. To suffer shame and ridicule, persecution and distress, poverty and 
    loss for Him now, is to wear the crown, and wave the palm, to swell the 
    triumph, and shout the song, when He shall descend the second time in glory 
    and majesty, to raise His Bride from the scene of her humiliation, robe her 
    for the marriage, and make her manifestly and eternally His own.<br>
    <br>
    Oh! laud His great name for all the present conduct of His providence and 
    grace. Praise Him for all the wise though affecting discoveries He gives you 
    of yourself, of the creature, of the world. Blessed, ah! truly blessed and 
    holy is the discipline that prostrates your spirit in the dust. There it is 
    that He reveals the secret of His own love, and draws apart the veil of His 
    own loveliness. There it is that He brings the soul deeper into the 
    experience of His sanctifying truth; and, with new forms of beauty and 
    expressions of endearment, allures the heart, and takes a fresh possession 
    of it for Himself. And there, too, it is that the love, tenderness, and 
    grace of the Holy Spirit are better known. As a Comforter, as a Revealer of 
    Jesus, we are, perhaps, more fully led into an acquaintance with the work of 
    the Spirit in seasons of soul-abasement than at any other time. The mode and 
    time of His divine manifestation are thus beautifully predicted: &quot;He shall 
    come down like rain on the mown grass; as showers that water the earth.&quot; 
    Observe the gentleness, the silence, and the sovereignty of His 
    operation--&quot;He shall come down like rain.&quot; How characteristic of the blessed 
    Spirit&rsquo;s grace! Then mark the occasion on which He descends--it is at the 
    time of the soul&rsquo;s deep prostration. The waving grass is mowed--the lovely 
    flower is laid low--the fruitful stem is broken--that which was beautiful, 
    fragrant, and precious is cut down--the fairest first to fade, the loveliest 
    first to die, the fondest first to depart; then, when the mercy is gone, and 
    the spirit is bowed, when the heart is broken, the mind is dejected, and the 
    world seems clad in wintry desolation and gloom, the Holy Spirit, in all the 
    softening, reviving, comforting, and refreshing influence of His grace, 
    descends, speaks of the beauty of Jesus, leads to the grace of Jesus, lifts 
    the bowed soul, and reposes it on the bosom of Jesus.<br>
    <br>
    Precious and priceless, then, beloved, are the seasons of a believer&rsquo;s 
    humiliation. They tell of the soul&rsquo;s emptiness, of Christ&rsquo;s fullness; of the 
    creature&rsquo;s insufficiency, of Christ&rsquo;s all-sufficiency; of the world&rsquo;s 
    poverty, of Christ&rsquo;s affluence; they create a necessity which Jesus 
    supplies, a void which Jesus fills, a sorrow which Jesus soothes, a desire 
    which Jesus satisfies. They endear the cross of the incarnate God, they 
    reveal the hidden glory of Christ&rsquo;s humiliation, they sweeten prayer, and 
    lift the soul to God; and then, &quot;truly our fellowship is with the Father, 
    and with His Son, Jesus Christ.&quot; Are you as a bruised flower? are you as a 
    broken stem? Does some heavy trial now bow you in the dust? Oh never, 
    perhaps, were you so truly beautiful--never did your grace send forth such 
    fragrance, or your prayers ascend with so sweet an odor--never did faith, and 
    hope, and love develop their hidden glories so richly, so fully as now! In 
    the eye of a wounded, a bruised, and a humbled Christ, you were never more 
    lovely, and to His heart never more precious than now--pierced by His hand, 
    smitten by His rod, humbled by His chastisement, laid low at His feet, 
    condemning yourself, justifying Him, taking to yourself all the shame, and 
    ascribing to Him all the glory.<br>
    <br>
    <br>
