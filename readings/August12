    AUGUST 12.<br>
    <br>
    &quot;For we who worship God in the Spirit are the only ones who are truly
    circumcised. We put no confidence in human effort. Instead, we boast about
    what Christ Jesus has done for us.&quot; Philippians 3:3<br>
    <br>
    The first step the Spirit takes in this great work is to lead us from
    ourselves- from all reliance on our own righteousness, and from all
    dependence upon our native strength. But let us not suppose that this
    divorce from the principle of self entirely takes place when we are &quot;married
    to another, even to Christ.&quot; It is the work of a life. Alas! Christ has at
    best but a portion of our affections. Our heart is divided. It is true,
    there are moments, br>ight and blissful, when we sincerely and ardently
    desire the full, unreserved surrender. But the ensnaring power of some rival
    object soon discovers to us how partial and imperfect that surrender has
    been. This severing from ourselves- from all our idols- is a perpetual,
    unceasing work of the Spirit. And who but this Divine Spirit could so lead
    us away from self, in all its forms, as to constrain us to trample all our
    own glory in the dust, and acknowledge with Paul that we are &quot;less than the
    least of all saints.&quot; <br>
    <br>
    But more than this, He leads from an opposite extreme of self- from a
    despairing view of our personal sinfulness. How often, when the eye has been
    intently bent within, gazing as it were upon the gloom and confusion of a
    moral chaos, the Spirit has gently and graciously led us from ourselves to
    an object, the sight of which has at once raised us from the region of
    despair! How many walk in painful and humiliating bondage, from not having
    thus been sufficiently led out of themselves! Always contemplating their
    imperfect repentance, or their weak faith, or their little fruitfulness,
    they seem ever to be moving in a circle, and to know nothing of what it is
    to walk in a large place. Thus from sinful self, as from righteous self, the
    Spirit of God leads us. <br>
    <br>
    To what does He lead? He leads us to Christ. To whom else would we, in our
    deep necessity, wish to be led? Now that we know something experimentally of
    Jesus, to whom would we go but to Him? Having severed us in some degree from
    ourselves, He would br>ing us into a closer realization of our union with the
    Savior. &quot;He will br>ing glory to me by taking from what is mine and making it
    known to you.&quot; <br>
    <br>
    And this promise is fulfilled when, in all our need, He leads us to Christ.
    Are we guilty? the Spirit leads us to the blood of Jesus. Are we weary? the
    Spirit leads us to abide in Jesus. Are we sorrowful? the Spirit leads us to
    the sympathy of Jesus. Are we tempted? the Spirit leads us to the protection
    of Jesus. Are we sad and desolate? the Spirit leads us to the tender love of
    Jesus. Are we poor, empty, and helpless? the Spirit leads us to the fullness
    of Jesus. And still it is to the Savior He conducts us. The Holy Spirit is
    our comforter, but the holy Jesus is our comfort. And to Jesus- to His
    person, to His offices, and to His work, in life and in death, the Divine
    Guide ever leads us.<br>
    <br>
    <br>
