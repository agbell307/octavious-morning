    AUGUST 25.<br>
    <br>
    &quot;What is the exceeding greatness of his power to us who believe, according
    to the working of his mighty power.&quot; Ephesians 1:19<br>
    <br>
    Divine power, not less than love, is a perfection we shall require at every
    step of our yet untried and unknown path. We shall have needs which none but
    the power that multiplied the five loaves to supply the hunger of the five
    thousand can meet; difficulties, which none but the power that asks, &quot;Is
    anything too hard for me? says the Lord,&quot; can overcome; enemies, with whom
    none but the power that resisted Satan, vanquished death, and br>oke from the
    grave, can cope. All this power is on our side, if our trust is in the Lord.
    &quot;All power is given unto me in heaven and in earth,&quot; exclaims Jesus. This
    power which the Lord exerts on our behalf, and in which He invites us to
    trust, is made perfect in weakness. <br>
    <br>
    Hence, we learn the same lesson that teaches us the utter lack of strength
    in ourselves. And when the Lord has reduced our confidence, and weakened our
    strength, as in the case of Gideon, whose army He reduced from thirty-two
    thousand men to three hundred, He then puts forth His power, perfects it in
    our weakness, gives us the victory, and secures to Himself all the praise.
    Go forward, relying upon the power of Jesus to do all in us, and accomplish
    all for us: power to subdue our sins; power to keep our hearts; power to
    uphold our steps; power gently to lead us over rough places, firmly to keep
    us in smooth places, skillfully to guide us through crooked paths, and
    safely to conduct us through all perils, fully to vindicate us from all
    assaults, and completely to cover our heads in the day of battle. Invincible
    is that soul thus clad in the panoply of Christ's power.<br>
    <br>
    The power which belongs to Him as God, and the power which He possesses as
    Mediator, is all exerted in the behalf of those who put their trust in Him.
    &quot;You have given Him power,&quot; are His own words, &quot;over all flesh, that He
    should give eternal life to as many as You have given Him.&quot; Child of God!
    gird yourself for duties, toils, and trials, &quot;strong in the grace that is in
    Christ Jesus.&quot; And when the stone of difficulty confronts you; lying,
    perhaps, heavily upon some buried mercy; hear Him ask you, before he rolls
    it quite away; &quot;Do you believe that I am able to do this?&quot; Oh, that your
    trusting heart may instantly respond, &quot;Yes, Lord, I believe, I trust; for
    with You all things are possible.&quot;<br>
    <br>
    <br>
