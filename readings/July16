    JULY 16.<br>
    <br>
    &quot;Only believe.&quot; Mark 5:36<br>
    <br>
    Precious and significant are the words of Jesus, the very same words that He 
    spoke when on earth. Did those lips, glowing with more than a seraph's 
    hallowed touch-lips into which grace without measure was poured-ever breathe 
    a sentence more touching, more simple, or more significant than this, &quot;Only 
    believe&quot;? Originally addressed to an afflicted parent, who sought His 
    compassion and His help in behalf of a little daughter lying at the point of 
    death, they seem to be especially appropriate to every case of anxiety, of 
    trial, and of need. Alas! how many such will scan this page-how many a sigh 
    will breathe over it, how many a tear will moisten it, how many a mournful 
    glance will light upon it! Be it so; there comes back a voice of sympathy 
    responsive to each sad heart-not man, but Jesus speaks-&quot;Only believe&quot;-in 
    other words, &quot;only trust.&quot; What is faith, but trust? what is believing in 
    Jesus, but trusting in Jesus? When Jesus says, &quot;only believe me,&quot; He 
    literally says, &quot;only trust me.&quot; And what a natural, beautiful, soothing 
    definition of the word faith is this! <br>
    <br>
    Many a volume has been written to explain the nature and illustrate the 
    operation of faith-the subject and the reader remaining as much mystified 
    and perplexed as ever. But who can fail to comprehend the meaning of the 
    good old Saxon word trust! All can understand what this means. When, 
    therefore, Jesus says-as He does to every individual who reads these 
    words-&quot;only believe me,&quot; He literally says, &quot;only trust me.&quot; Thus He spoke 
    to the anxious father who besought Him to come and heal his child: &quot;only 
    believe-only trust my power, only trust my compassion, only trust my word; 
    do not be afraid, only trust me.&quot; And thus He speaks to you, believer. Oh, 
    for a heart to respond, &quot;Speak, Lord, for your servant hears!&quot;<br>
    <br>
    Trust implies, on our part, mystery and ignorance, danger and helplessness. 
    How wrapped in inscrutability, how shadowy and unreal, is all the future! As 
    we attempt to penetrate the dark clouds, what strange forebodings steal over 
    our spirits. Just at this juncture Jesus approaches, and with address most 
    winning, and in accents most gentle, speaks these words, &quot;Only believe-only 
    trust me! Trust me, who knows the end from the beginning; trust me, who has 
    all resources at my command; trust me, whose love never changes, whose 
    wisdom never misleads, whose word never fails, whose eye never slumbers nor 
    sleeps-only trust me!&quot; Enough, my blessed Lord, my soul replies. I will sit 
    myself down a loving child, a lowly disciple at Your feet, and, indistinct 
    and dreary as my future path may be, will learn from You how and where I may 
    trust You all my journey through.<br>
    <br>
    <br>
