    OCTOBER 23.<br>
    <br>
    &quot;The just shall live by faith.&quot; Hebrews 10:38<br>
    <br>
    THE experience of every believer is, in a limited degree, the experience of 
    the great apostle of the Gentiles, the tip of whose soaring pinion we, who 
    so much skim the earth's surface, can scarcely touch-&quot;The life which I now 
    live in the flesh, I live by the faith of the Son of God.&quot; &quot;Like precious 
    faith&quot; with his dwells in the hearts of all the regenerate. Along this royal 
    highway it is ordained of God that all His people should travel. It is the 
    way their Lord traveled before them; it is the way they are to follow after 
    Him. The first step they take out of the path of sense is into the path of 
    faith. And what a mighty grace do they find it, as they journey on! Do they 
    live? it is by faith. Hebrews 10:38. Do they stand? it is by faith. Romans 
    11:20. Do they walk? it is by faith. 2 Corinthians 5:7. Do they fight? it is 
    by faith. 1 Timothy 6:12. Do they overcome? it is by faith. 1 John 5:4. Do 
    they see what is invisible? it is by faith. Hebrews 6:27. Do they receive 
    what is incredible? it is by faith. Romans 4:20. Do they achieve what is 
    impossible? it is by faith. Mark 9:23. Glorious achievements of faith!<br>
    <br>
    And, oh, how eminently is Jesus thus glorified in His saints! Was it no 
    glory to Joseph, that, having the riches of Egypt in his hands, all the 
    people were made, as it were, to live daily and hourly upon him? Was no 
    fresh accession of glory brought to his exaltation, by every fresh 
    acknowledgment of his authority, and every renewed application to his 
    wealth? And is not Jesus glorified in His exaltation and in His fullness, in 
    His love and in His grace, by that faith, in the exercise of which &quot;a poor 
    and afflicted people,&quot; a needy and a tried Church, are made to travel to, 
    and live upon, Him each moment? Ah, yes! every corruption taken to His 
    sanctifying grace, every burden taken to his omnipotent arm, every sorrow 
    taken to His sympathizing heart, every want taken to His overflowing 
    fullness, every wound taken to His healing hand, every sin taken to His 
    cleansing blood, and every deformity taken to His all-covering 
    righteousness, swells the revenue of glory which each second of time ascends 
    to our adorable Redeemer from His Church. You may have imagined-for I will 
    now suppose myself addressing a seeking soul-that Christ has been more 
    glorified by your hanging back from Him-doubting the efficacy of His blood 
    to cancel your guilt, the power of His grace to mortify your corruption, the 
    sufficiency of His fullness to supply your need, the sympathy of His nature 
    to soothe your grief, and the loving willingness of His heart to receive and 
    welcome you as you are, empty, vile, and worthless; little thinking, on the 
    contrary, how much He has been grieved and wounded, dishonored and robbed of 
    His glory, by this doubting of His love, and this distrusting of His grace, 
    after all the melting exhibitions of the one, and all the convincing 
    evidences of the other. But, is it the desire of your inmost soul that 
    Christ should be glorified by you? Then do not forget the grand, luminous 
    truth of the Bible, that He is the Savior of sinners, and of sinners as 
    sinners-that, in the great matter of the soul's salvation, He recognizes 
    nothing of worthiness in the creature; and that whatever human merit is 
    brought to Him with a view of commending the case to His notice-whatever-be 
    it even the incipient work of His own Spirit in the heart-is appended to His 
    finished work, as a ground of acceptance with God, is so much detraction 
    from His glory as a Redeemer-than which, of nothing is He more jealous-and 
    consequently, places the soul at a great remove from His grace. But like 
    Bartimeus, casting the garment from you, be that garment what it may-pride 
    of merit, pride of intellect, pride of learning, pride of family, pride of 
    place, yes, whatever hinders your entering the narrow way, and prevents your 
    receiving the kingdom of God &quot;as a little child,&quot; and coming to Jesus to be 
    saved by Him alone-brings more real glory to Him than imagination can 
    conceive, or words can describe.<br>
    <br>
    <br>
