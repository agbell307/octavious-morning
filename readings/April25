    <p align="justify">APRIL 25. </p>
    <p align="justify">&quot;Do you believe on the Son of God?&quot; John 9:35 </p>
    <p align="justify">The application of this question, reader, must be to your 
    conscience. Have you &quot;like precious faith&quot; with that which we have attempted 
    to describe? Alas! it may be that you are that tree which brings not forth 
    this good fruit. Yours may be a species of fruit somewhat resembling it; but 
    do not be deceived in a matter so momentous as this. &quot;You believe that there 
    is one God; you do well: the devils also believe, and tremble.&quot; That is, you 
    assent to the first proposition of true religion- the being of God; this is 
    well, because your judgment assents to that which is true. And still you 
    have not gone beyond the faith of demons! They believe, and yet horror 
    inconceivable is but the effect of the forced assent of their minds to the 
    truth- they &quot;tremble.&quot; Oh, look well to your faith! There must be, in true 
    faith, not only an assent, but also a consent. In believing to the saving of 
    the soul, we not only assent to the truth of the word, but we also consent 
    to take Christ as He is there set forth- the sinner's reconciliation with 
    God. A mere intellectual illumination, or a historical belief of the facts 
    of the Bible, will never place the soul beyond the reach of hell, nor within 
    the region of heaven. There is a &quot;form of knowledge,&quot; as well as a &quot;form of 
    godliness;&quot; and both existing apart from vital religion in the soul 
    constitute a &quot;vain religion.&quot; Again we press upon you the important inquiry, 
    Have you the &quot;faith of God's elect&quot;? Is it a faith that has stained the 
    glory of self-merit, and laid the pride of intellect in the dust? Is it 
    rooted in Christ? Has it transformed you, in some degree, into the opposite 
    of what you once were? Are any of the &quot;precious fruits&quot; of the Spirit put 
    forth in your life? Is Jesus precious to your soul? And to walk in all 
    circumstances humbly with God- is it the earnest desire of your heart? If 
    there is no sorrow for sin, no going out of yourself to Jesus, no fruits of 
    holiness, in some degree, appearing, then is yours but a &quot;dead faith,&quot;- 
    dead, because it is a part and parcel of a nature &quot;dead in trespasses and in 
    sins,&quot;- dead, because it is not the fruit of the quickening Spirit- dead, 
    because it is inoperative, even as the lifeless root transmits no vitality 
    and moisture to the tree- dead, because it never can bring you to eternal 
    life. Of what value, then, is it? Cut it down! why should it use up the 
    ground? If, then, you have never brought forth the good fruit of prayer, and 
    repentance, and Faith, you are yet in the old nature of sin of rebellion, 
    and of death. <br>
&nbsp; </p>
