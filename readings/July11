    JULY 11.<br>
    <br>
    &quot;I am crucified with Christ; nevertheless I live; yet not I, but Christ 
    lives in me.&quot; Galatians 2:20.<br>
    <br>
    The life of Christ and the life of self cannot coexist in the same heart. If 
    the one lives, the other dies. The sentence of death is written upon a man's 
    self, when the Spirit of Christ enters his heart, and quickens his soul with 
    the life of God. &quot;I live,&quot; he exclaims, &quot;yet not I.&quot; What a striking and 
    beautiful example of this have we in the life and labors of the apostle 
    Paul! Does he speak of his ministry?-what a renunciation of self appears! 
    Lost in the greatness and grandeur of his theme, he exclaims-&quot;We preach not 
    ourselves, but Christ Jesus the Lord.&quot; Again-&quot;Unto me who am less than the 
    least of all saints, is this grace given, that I should preach among the 
    Gentiles the unsearchable riches of Christ.&quot; Does he refer to his 
    office?-what self-crucifixion! &quot;I magnify my office.&quot; In what way? Was it by 
    vaunting proclamations of its grandeur and legitimacy, its Divine 
    institution, or its solemn functions? Never! but he magnified his office by 
    diminishing himself, and exalting his Master. He was nothing-aye, and even 
    his office itself was comparatively nothing-that &quot;Christ might be all in 
    all.&quot; Does he speak of his gifts and labors? what absence of self! &quot;I am the 
    least of the apostles, that am not fit to be called an apostle, because I 
    persecuted the Church of God. But by the grace of God I am what I am; and 
    His grace, which was bestowed upon me, was not in vain, but I labored more 
    abundantly than they all: yet not I, but the grace of God which was with 
    me.&quot; Such was the religion of Paul. His Christianity was a self-denying, 
    self-crucifying, self-renouncing Christianity. &quot;I live, yet not I. I labored 
    more abundantly than they all, yet not I.&quot; Oh what a self-denying spirit was 
    his!<br>
    <br>
    But every truly spiritual man is a self-renouncing man. In the discipline of 
    his own heart, beneath the cross of Jesus, and in the school of trial and 
    temptation, he has been taught in some degree, that if he lives, it is not 
    he that lives, but that it is Christ that lives in him. Upon all his own 
    righteousness, his duties, and doings, he tramples as to the great matter of 
    justification; while, as fruits of the Spirit, as evidences of faith, as 
    pulsations of the inner spiritual life, as, in a word, tending to 
    authenticate and advance his sanctification, he desires to be &quot;careful to 
    maintain good works,&quot; that God in all things might be glorified.<br>
    <br>
    <br>
