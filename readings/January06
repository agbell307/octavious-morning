    <p align="justify">JANUARY 6. </p>
    <p align="justify">&quot;In the world you shall have tribulation.&quot; John 16:33.
    </p>
    <p align="justify">Could we draw aside, for a moment, the thin veil that 
    separates us from the glorified saints, and inquire the path along which 
    they were conducted by a covenant God to their present enjoyments, how few 
    exceptions, if any, would we find to that declaration of Jehovah- &quot;I have 
    chosen you in the furnace of affliction.&quot; All world tell of some peculiar 
    cross; some domestic, relative, or personal trial which attended them every 
    step of their journey; which made the valley they trod, truly, &quot;a valley of 
    tears,&quot; and which they only threw off when the spirit, divested of its robe 
    of flesh, fled where sorrow and sighing are forever done away. God's people 
    are a sorrowful people. The first step they take in the divine life is 
    connected with tear's of godly sorrow; and, as they, travel on, sorrow and 
    tears do but trace their steps. They sorrow over the body of sin which they 
    are compelled to carry with them; they sorrow over their perpetual proneness 
    to depart, to backslide, to live below their high and holy calling. They 
    mourn that they mourn so little; they, weep that they weep so little; that 
    over so much indwelling sin, over so many and so great departures, they yet 
    are found so seldom mourning in the posture of one low in the dust before 
    God. In connection with this, there is the sorrow which results from the 
    needed discipline which the correcting hand of the Father who loves them 
    almost daily employs. For, in what light are all their afflictions to be 
    viewed, but as so many correctives, so much discipline employed by their God 
    in covenant, in order to make them &quot;partakers of His holiness.&quot; Viewed in 
    any other light, God is dishonored, the Spirit is grieved, and the believer 
    is robbed of the great spiritual blessing for which the trial was sent. <br>
&nbsp; </p>
