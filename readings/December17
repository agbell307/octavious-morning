    DECEMBER 17.<br>
    <br>
    &quot;Being justified freely by his grace through the redemption that is in 
    Christ Jesus; whom God has set forth to be a propitiation through faith in 
    his blood.&quot; Romans 3:24, 25<br>
    <br>
    By a change of place with the Church, Christ becomes the &quot;Lord our 
    Righteousness,&quot; and we are &quot;made the righteousness of God in Him.&quot; There is 
    the transfer of sin to the innocent, and, in return, there is the transfer 
    of righteousness to the guilty. In this method of justification, no violence 
    whatever is done to the moral government of God. So far from a shade 
    obscuring its glory, that glory beams forth with an effulgence which must 
    have remained forever veiled, but for the redemption of man by Christ. God 
    never appears so like Himself as when He sits in judgment upon the person of 
    a sinner, and determines his standing before Him upon the ground of that 
    satisfaction to His law rendered by the Son of God in the room and stead of 
    the guilty. Then does He appear infinitely holy, yet infinitely gracious; 
    infinitely just, yet infinitely merciful. Love, as if it had long been 
    panting for an outlet, now leaps forth and embraces the sinner; while 
    justice, holiness, and truth gaze upon the wondrous spectacle with infinite 
    complacence and delight. And shall we not pause and bestow a thought of 
    admiration and gratitude upon Him, who was constrained to stand in our place 
    of degradation and woe, that we might stand in His place of righteousness 
    and glory? What wondrous love! what stupendous grace! that He should have 
    been willing to have taken upon Him our sin, and curse, and woe! The 
    exchange to Him how humiliating! He could only raise us by Himself stooping. 
    He could only emancipate us by wearing our chain. He could only deliver us 
    from death by Himself dying. He could only invest us with the spotless robe 
    of His pure righteousness by wrapping around Himself the leprous mantle of 
    our sin and curse. Oh, how precious ought He to be to every believing heart! 
    What affection, what service, what sacrifice, what devotion, He deserves at 
    our hands! Lord, incline my heart to yield itself supremely to You! But in 
    what way does this great blessing of justification become ours? In other 
    words, what is the instrument by which the sinner is justified? The answer 
    is at hand, in the text, &quot;through faith in His blood.&quot; Faith, and faith 
    alone, makes this righteousness of God ours. &quot;By Him all that believe are 
    justified.&quot; And why is it solely and exclusively by faith? The answer is at 
    hand, &quot;Therefore it is of faith, that it might be by grace.&quot; Were 
    justification through any other medium than by believing, then the perfect 
    freeness of the blessing would not be secured. The expressions are, 
    &quot;Justified freely by His grace;&quot; that is, gratuitously--absolutely for 
    nothing. Not only was God in no sense whatever bound to justify the sinner, 
    but the sovereignty of His law, as well as the sovereignty of His love, 
    alike demanded that, in extending to the sinner the greatest boon of His 
    government, He should do so upon no other principle than as a perfect act of 
    grace on the part of the Giver, and as a perfect gratuity on the part of the 
    recipient--having &quot;nothing to pay.&quot; Therefore, whatever is associated with 
    faith in the matter of the sinner&rsquo;s justification--whether it be baptism, or 
    any other rite, or any work or condition performed by the creature--renders 
    the act entirely void and of none effect. The justification of the believing 
    sinner is as free as the God of love and grace can make it.<br>
    <br>
    <br>
