    OCTOBER 3.<br>
    <br>
    &quot;Now unto him that is able to keep you from falling, and to present you 
    faultless before the presence of his glory with exceeding joy, to the only 
    wise God our Savior, be glory and majesty, dominion and power, both now and 
    ever. Amen.&quot; Jude 24, 25<br>
    <br>
    WHAT is the great evil of which the true saints of God most stand in 
    jeopardy, and which their timid, fearful hearts most dread? Is it not secret 
    and outward backsliding from God after conversion? Surely it is, as the 
    experience of every honest, upright, God-fearing man will testify. It is his 
    consolation, then, to know that Jesus is &quot;able to keep him from falling.&quot; 
    This is the most overwhelming evil that stares the believer in the face. 
    Some, but imperfectly taught in the word, are dreading awful apostasy from 
    the faith here, and final condemnation from the presence of God 
    hereafter-believing that though Christ has made full satisfaction for their 
    sins to Divine justice, has cancelled the mighty debt, has imputed to them 
    His righteousness, has blotted out their iniquities, has called, renewed, 
    sanctified, and taken full possession of them by His Spirit, and has 
    ascended up on high, to plead their cause with the Father-that yet, after 
    all this stupendous exercise of power, and this matchless display of free 
    grace, they may be left to utter apostasy from God, and be finally and 
    eternally lost. If there is one doctrine more awful in it nature, 
    distressing in its consequences, and directly opposed to the glory of God 
    and the honor of Christ, than another, methinks it is this. Others, again, 
    more clearly taught my the Spirit, are heard to say, &quot;I believe in the 
    stability of the covenant, in the unchangeableness of God's love, and in the 
    faithfulness of my heavenly Father; but I fear lest some day under some 
    sharp temptation-some burst of indwelling sin, when the enemy shall come in 
    as a flood-I shall fall, to the wounding of my peace, to the shame of my 
    brethren, and to the dishonoring of Christ.&quot; Dear believer, truly you would 
    fall, were He to leave you to your own keeping for one moment; but Jesus is 
    able to keep you from falling. Read the promises, believe them, rest upon 
    them. A simple glance will present to the believer's eye a threefold cord, 
    by which he is kept from falling. In the first place, God the Father keeps 
    him-&quot;kept by the power of God;&quot; the power that created and upholds the world 
    keeps the believer. The eternal purpose, love, and grace of the Father keeps 
    him: this is the first cord. Again, God the Son keeps him: &quot;My sheep hear my 
    voice, and I know them, and they follow me; and I give unto them eternal 
    life; and they shall never perish, neither shall any man pluck them out of 
    my hand.&quot; The covenant engagements, the perfect obedience, the atoning death 
    of Immanuel, keep the believer: this is the second cord. Yet again, God the 
    Holy Spirit keeps him: &quot;When the enemy shall come in like a flood, the 
    Spirit of the Lord shall lift up a standard against him&quot; (marg. shall put 
    him to flight). The tender love, the covenant faithfulness, and the 
    omnipotent power of the Eternal Spirit keep the believer: this is the third 
    cord. And &quot;a threefold cord is not quickly broken.&quot; But with these promises 
    of the triune God to keep His people from falling, He has wisely and 
    graciously connected the diligent, prayerful use of all the means which He 
    has appointed for this end.<br>
    <br>
    <br>
