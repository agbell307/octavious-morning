    SEPTEMBER 29.<br>
    <br>
    &quot;For I have given you an example, that you should do as I have done to you.&quot; 
    John 13:15<br>
    <br>
    EVERY soul re-cast into this model, every mind conformed to this pattern, 
    and every life reflecting this image, is an exalting and a glorifying of the 
    Son of God. There is no single practical truth in the word of God on which 
    the Spirit is more emphatic than the example which Christ has set for the 
    imitation of His followers. The Church needed a perfect pattern, a flawless 
    model. It wanted an impersonation, a living embodiment of those precepts of 
    the gospel so strictly enjoined upon every believer, and God has graciously 
    set before us our true model. &quot;Whom He did foreknow, He also did 
    predestinate to be conformed to the image of His Son.&quot; And what says Christ 
    Himself? &quot;My sheep follow me.&quot; We allow that there are points in which we 
    cannot and are not required literally and strictly to follow Christ. We 
    cannot lay claim to His infallibility. He who sets himself up as infallible 
    in his judgment, spotlessly pure in his heart, and perfect in his 
    attainments in holiness, deceives his own soul. Jesus did many things, too, 
    as our Surety, which we cannot do. We cannot drink of the cup of Divine 
    trembling which He drank; nor can we be baptized with the baptism of blood 
    with which He was baptized. He did many things as a Jew-was circumcised, 
    kept the passover &amp;christian.-which are not obligatory upon us. And yet, in 
    all that is essential to our sanctification, to our holy, obedient, 
    God-glorifying walk, He has &quot;left us an example, that we should follow His 
    steps.&quot; In His lowly spirit, meek, humble deportment, and patient endurance 
    of suffering: &quot;Learn of me, for I am meek and lowly in heart.&quot; In the 
    disinterestedness of His love, His pure benevolence, the unselfishness of 
    His religion: &quot;Look not every man on his own things, but every man also on 
    the things of others: let this mind be you which was also in Christ Jesus.&quot; 
    &quot;For even Christ pleased not Himself.&quot; Look not every man on his own circle, 
    his own family, his own gifts, his own interests, comfort, and happiness; 
    upon his own Church, his own community, his own minister. Let him not look 
    upon these exclusively. Let him not prefer his own advantage to the public 
    good. Let him not be self-willed in matters involving the peace and comfort 
    of others. Let him not form favorite theories, or individual opinions, to 
    the hazard of a Church's prosperity or of a family's happiness. Let him 
    yield, sacrifice, and give place, rather than carry a point to the detriment 
    of others. Let him, with a generous, magnanimous, disinterested spirit, in 
    all things imitate Jesus, who &quot;pleased not Himself.&quot; Let him seek the good 
    of others, honoring their gifts, respecting their opinions, nobly yielding 
    when they correct and overrule his own. Let him promote the peace of the 
    Church, consult the honor of Christ, and seek the glory of God, above and 
    beyond all private and selfish ends. This is to be conformed to the image of 
    God's dear Son, to which high calling we are predestinated; and in any 
    feature of resemblance which the Holy Spirit brings out in the holy life of 
    a follower of the Lamb, Christ is thereby glorified before men and angels.<br>
    <br>
    <br>
