    <p align="justify">MARCH 31. </p>
    <p align="justify">&quot;And for sin, condemned sin in the flesh.&quot; Romans 8:3.
    </p>
    <p align="justify">As sin is the great condemning cause, let us aim to 
    condemn sin, if we would rank with those for whom there is no condemnation. 
    Most true is it, that either sin must be condemned by us, or we must be 
    condemned for sin. The honor of the Divine government demands that a 
    condemnatory sentence be passed, either upon the transgression, or upon the 
    transgressor. And shall we hesitate? Is it a matter of doubt to which our 
    preference shall be given? Which is best, that sin should die, or that we 
    should die? Will the question allow a moment's consideration? Surely not, 
    unless we are so enamored with sin as calmly and deliberately to choose 
    death rather than life, hell rather than heaven. &quot;The wages of sin is 
    death.&quot; Sin unrepented, unforgiven, unpardoned, is the certain prelude to 
    eternal death. Everlasting destruction follows in its turbid wake. There is 
    a present hell in sin, for which the holy shun it; and there is a future 
    hell in sin, for which all should dread it. If, then, we would be among &quot;the 
    pure in heart who shall see God,&quot; if we would lift up our faces with joy 
    before the Judge at the last great day, if we would be freed from the final 
    and terrible sentence of condemnation, oh, let us be holy, &quot;denying all 
    ungodliness and worldly lusts, and living righteously, soberly, and godly in 
    this present world.&quot; Oh, let us condemn sin, that sin may not condemn us. 
    And let us draw the motive that constrains us, and the power that helps us, 
    from that cross where Jesus &quot;condemned sin in the flesh.&quot;</p>
