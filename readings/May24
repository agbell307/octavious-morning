    <p align="justify">MAY 24. </p>
    <p align="justify">&quot;Jesus said unto them, Verily, verily, I say unto you, 
    Before Abraham was, I am.&quot; John 8:58 </p>
    <p align="justify">Dear reader, what a wondrous declaration is this! What a 
    glorious and precious truth does it involve! Are you a believer in Jesus? Is 
    He all your salvation, your acceptance, your hope, and desire? Then cast the 
    anchor of your faith deeply, firmly here; you shall find it an eternal rock. 
    Weak faith you may have, and doubtful faith numbers have; but here is the 
    ground of faith, respecting which there can be neither weakness nor doubt. 
    Is it an Almighty Savior that you need? Behold Him! &quot;Before Abraham was, I 
    am.&quot; Oh, what a foundation for a poor sinful worm of the dust to build upon! 
    What a stable truth for faith in its weakest form to deal with- to have a 
    glorious incarnate 'I Am' for an atoning sacrifice- an 'I Am' for a 
    Redeemer- an 'I Am' for a Surety- an 'I Am' as a Day's-man between God and 
    the soul- an 'I Am' as an Advocate, an unceasing Intercessor at the court of 
    heaven, pleading each moment His own atoning merits- an 'I Am' as the center 
    in whom all the promises are &quot;yes and amen&quot;- an 'I Am' as a &quot;Brother born 
    for adversity&quot;- an 'I Am' as &quot;a very present help in trouble&quot;! This is the 
    answer which faith receives to its trembling and anxious interrogatories. To 
    each and all touching His faithfulness, His tenderness, His long-suffering, 
    His fulness, and His all-sufficiency, Jesus answers, &quot;I Am.&quot; &quot;Enough, Lord,&quot; 
    replies the believer, &quot;on this I can live, on this I can die.&quot; </p>
