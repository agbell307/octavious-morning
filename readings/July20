    JULY 20.<br>
    <br>
    &quot;Except you repent, you shall all likewise perish.&quot; Luke 13:5<br>
    <br>
    This was the doctrine which our Lord preached; and so did His apostles, when 
    they declared, &quot;God now commands all men everywhere to repent.&quot; No command, 
    no duty, can be more distinctly, intelligently, and solemnly defined and 
    urged than this. But the inquirer will ask, &quot;What is repentance?&quot; The reply 
    is-it is that secret grace that lays the soul low before God-self loathed; 
    sin abhorred, confessed, and forsaken. It is the abasement and humiliation 
    of a man, because of the sinfulness of his nature and the sins of his life, 
    before the holy, heart-searching Lord God. The more matured believer is wont 
    to look upon a broken and contrite spirit, flowing from a sight of the 
    cross, as the most precious fruit found in his soul. No moments to him are 
    so hallowed, so solemn, or so sweet, as those spent in bathing the Savior's 
    feet with tears. <br>
    <br>
    There is indeed a bitterness in the grief which a sense of sin produces; and 
    this, of all other bitterness, is the greatest. He knows, from experience, 
    that it is an &quot;evil thing and bitter, that he has forsaken the Lord his 
    God.&quot; Nevertheless, there is a sweetness, an indescribable sweetness, which 
    must be experienced to be understood, blended with the bitterness of a heart 
    broken for sin, from a sight of the cross of the incarnate God. Oh, precious 
    tears wept beneath that cross! <br>
    <br>
    &quot;For thus says the high and lofty One who inhabits eternity, whose name is 
    Holy; I dwell in the high and holy place, with him also that is of a 
    contrite and humble spirit, to revive the spirit of the humble, and to 
    revive the heart of the contrite ones.&quot; But how shall I portray the man who 
    is of a contrite and humble spirit? He is one who truly knows the evil of 
    sin, for he has felt it. He apprehends, in some degree, the holiness of 
    God's character, and the spirituality of His law, for he has seen it. His 
    views of himself have undergone a radical change. He no longer judges of 
    himself as others judge of him. They exalt him; he abases himself. They 
    approve; he condemns. And in that very thing for which they most extol him 
    he is humbling himself in secret. While others are applauding actions, he is 
    searching into motives; while they are extolling virtues, he is sifting 
    principles; while they are weaving the garland for his brow, he, shut in 
    alone with God, is covering himself with sackcloth and with ashes.<br>
    <br>
    Oh precious fruit of a living branch of the true vine! Is it any wonder, 
    then, that God should come and dwell with such a one, in whom is found 
    something so good towards Him? Oh, no! He delights to see us in this 
    posture-to mark a soul walking before Him in a conscious sense of its 
    poverty; the eye drawing from the cross its most persuasive motives to a 
    deep prostration of soul at His feet. Dear reader, to know what a sense of 
    God's reconciling love is-to know how skillfully, tenderly, and effectually 
    Jesus binds up and heals-your spirit must be wounded, and your heart must be 
    broken for sin. Oh, it were worth an ocean of tears to experience the loving 
    gentleness of Christ's hand in drying them. Has God ever said of you, as He 
    said of Ahab, &quot;See how he humbles himself before me?&quot; Search and ascertain 
    if this good fruit is found in your soul.<br>
    <br>
    <br>
