    <p align="justify">JANUARY 12. </p>
    <p align="justify">&quot;With You is the fountain of life.&quot;&nbsp; Psalm 36:9. </p>
    <p align="justify">Behold, what a fountain of life is God! All 
    intelligences, from the highest angel in heaven to the lowliest creature on 
    earth, drawing every breath of their existence from Him. &quot;In Him we live, 
    and move, and have our being.&quot; But He is more than this to the Church. He is 
    the fountain of love, as well as of life. The spirits of &quot;just men made 
    perfect,&quot; and the redeemed on earth, satiate their thirsty souls at the 
    overflowing fullness of the Father's love. How much do we need this truth! 
    What stinted views, unjust conceptions, and wrong interpretations have we 
    cherished of Him, simply because we overlook His character as the Fountain 
    of living waters! We &quot;limit the Holy One of Israel.&quot; We judge of Him by our 
    poor, narrow conception of things. We think that He is such a one as we 
    ourselves are. We forget, in our approaches, that we are coming to an 
    Infinite Fountain. That the heavier the demand we make upon God, the more we 
    shall receive, and that the oftener we come, the more are we welcome. That 
    we cannot ask too much. That our sin and His dishonor are, that we ask so 
    little. We forget that He is glorified in giving; and that the more grace He 
    metes out to His people, the richer the revenue of praise which He receives 
    in return. How worthy of such an infinite Fountain of love and grace is His 
    &quot;unspeakable gift.&quot; It came from a large heart; and the heart that gave 
    Jesus will withhold no good thing from those who walk uprightly. <br>
&nbsp; </p>
