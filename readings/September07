    SEPTEMBER 7.<br>
    <br>
    &quot;Have you received the Holy Spirit since you believed?&quot;<br>
    <br>
    WHAT the Church of God needs as a Church we equally need as individual 
    Christians-the deeper baptism of the Holy Spirit. Reader, why is it that you 
    are not more settled in the truth-your feet more firm upon the Rock? Why are 
    you not more rejoicing in Christ Jesus, the pardoning blood more sensibly 
    applied to the conscience, the seal of adoption more deeply impressed upon 
    your heart, &quot;Abba, Father&quot; more frequently, and with stronger, sweeter 
    accent, on your lips? Why are you, perhaps, so yielding in temptation, so 
    irresolute in purpose, so feeble in action, so vacillating in pursuit, so 
    faint in the day of adversity? Why is the glory of Jesus so dimly seen, His 
    preciousness so little felt, His love so imperfectly experienced? Why is 
    there so little close, secret transaction between God and your soul?-so 
    little searching of heart, confession of sin, dealing with the atoning 
    blood? Why does the conscience so much lack tenderness, and the heart 
    brokenness, and the spirit contrition? And why is the throne of grace so 
    seldom resorted to, and prayer itself felt to be so much a duty, and so 
    little a privilege, and, when engaged in, so faintly characterized with the 
    humble brokenness of a penitent sinner, the filial boldness of an adopted 
    child, the rich anointing of a royal priest? Ah! let the small measure in 
    which you have received the Holy Spirit's influence supply the answer. &quot;Have 
    you received the Holy Spirit since you believed?&quot;-have you received Him as a 
    Witness, as a Sealer, as a Teacher, as an Indweller, as a Comforter, as the 
    Spirit of adoption? But, rather, have you not forgotten that your Lord was 
    alive, and upon the throne exalted, to give you the Holy Spirit, and that 
    more readily than a father is to give good gifts to his child? That He is 
    prepared now to throw back the windows of heaven, and pour down upon you 
    such a blessing as shall confirm your faith, resolve your doubts, annihilate 
    your fears, arm you for the fight, strengthen you for the trial, give you an 
    unclouded view of your acceptance in the Beloved, and assure you that your 
    &quot;name is written among the living in Jerusalem&quot;? Then, as you value the 
    light of God's countenance, as you desire to grow in a knowledge of Christ, 
    as you long to be more &quot;steadfast, unmoveable, always abounding in the work 
    of the Lord,&quot; oh, seek to enjoy, in a larger degree, the presence, the love, 
    the anointing of the Holy Spirit. Christ has gone up on high to give to you 
    this invaluable blessing, and says for your encouragement, &quot;Hitherto have 
    you asked nothing in my name: ask, and you shall receive, that your joy may 
    be full.&quot;<br>
    <br>
    <br>
    September 8.<br>
    <br>
    &quot;But the natural man receives not the things of the Spirit of God: for they 
    are foolishness unto him: neither can he know them, because they are 
    spiritually discerned.&quot; 1 Corinthians 2:14<br>
    <br>
    THE mere presentation of truth to the unrenewed mind, either in the form of 
    threatening, or promise, or motive, can never produce any saving or 
    sanctifying effect. The soul of man, in its unrenewed state, is represented 
    as spiritually dead; insensible to all holy, spiritual motion. Now, upon 
    such a mind what impression is to be produced by the mere holding up of 
    truth before its eye? What life, what emotion, what effect will be 
    accomplished? As well might we spread out the pictured canvas before the 
    glazed eye of a corpse, and expect that by the beauty of the design, the 
    brilliancy of the coloring, and the genius of the execution, we would 
    animate the body with life, heave the bosom with emotion, and cause the eye 
    to swim with delight, as to look for similar moral effects to result from 
    the mere holding up to view divine truth before a carnal mind, &quot;dead in 
    trespasses and sins.&quot; And yet there are those who maintain the doctrine, 
    that divine truth, unaccompanied by any extraneous power, can effect all 
    these wonders! Against such a theory we would simply place one passage from 
    the sacred word: &quot;Except a man be born again, he cannot see the kingdom of 
    God.&quot; The sacred word, inspired though it be, is but a dead letter, 
    unclothed with the life-giving power of the Holy Spirit. Awful as are the 
    truths it unfolds, solemn as are the revelations it discloses, touching as 
    are the scenes it portrays, and persuasive as are the motives it supplies, 
    yet, when left to its own unaided operation, divine truth is utterly 
    impotent to the production of spiritual life, love, and holiness in the soul 
    of man. Its influence must necessarily be passive, possessing, as it does, 
    no actual power of its own, and depending upon a divine influence extraneous 
    from itself, to render its teaching efficacious. The three thousand who were 
    converted on the day of Pentecost were doubtless awakened under one sermon, 
    and some would declare it was the power of the truth which wrought those 
    wonders of grace. With this we perfectly agree, only adding, that it was 
    truth in the mighty hand of God which pricked them to the heart, and wrung 
    from them the cry, &quot;Men and brethren, what shall we do?&quot; The Eternal Spirit 
    was the efficient cause, and the preached truth but the instrument employed 
    to produce the effect; but for His accompanying and effectual power, they 
    would, as multitudes do now, have turned their backs upon the sermon of 
    Peter, though it was full of Christ crucified, deriding the truth, and 
    rejecting the Savior of whom it spoke. But it pleased God, in the 
    sovereignty of His will, to call them by His grace, and this He did by the 
    effectual, omnipotent power of the Holy Spirit, through the instrumentality 
    of a preached gospel.<br>
    <br>
    Thus, then, we plead for a personal experimental acquaintance with, and 
    reception of, the truth, before it can produce anything like holiness in the 
    soul. That it has found an entrance to the judgment merely will not do; 
    advancing not further-arresting not the will, touching not the heart, 
    renewing not the whole soul-it can never erect the empire of holiness in 
    man; the reign of sanctification cannot have commenced. The mental eye may 
    be clear, the moral eye closed; the mind all light, the heart all dark; the 
    creed orthodox, and the whole life a variance with the creed. Such is the 
    discordant effect of divine truth, simply settled in the human 
    understanding, unaccompanied by the power of the Holy Spirit in the heart. 
    But let a man receive the truth in the his heart by the power of God 
    Himself; let it enter there, disarming and dethroning the strong man; let 
    Jesus enter, and the Holy Spirit take possession, renewing, sealing, and 
    sanctifying the soul; and then we may look for the &quot;fruits of holiness, 
    which are unto eternal life.&quot;<br>
    <br>
    <br>
