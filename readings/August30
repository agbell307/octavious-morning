    AUGUST 30.<br>
    <br>
    &quot;Now no chastening for the present seems to be joyous, but grievous.&quot;
    Hebrews 12:11<br>
    <br>
    There is often a severity, a grievousness in the chastisements of our
    covenant God, which it is important and essential for the end for which they
    were sent, not to overlook. He who sent the chastisement appointed its
    character– He intended that it should be felt. There is as much danger in
    underrating as in overrating the chastisements of God. It is not uncommon to
    hear some of God's saints remark, in the very midst of His dealings with
    them, &quot;I feel it to be no cross at all; I do not feel it an affliction; I am
    not conscious of any peculiar burden.&quot; <br>
    <br>
    Is it not painful to hear such expressions from the lips of a dear child of
    God? It betrays a lack, so to speak, of spiritual sensitiveness; a
    deficiency of that tender, acute feeling which ought ever to belong to him
    who professes to have reposed on Jesus' bosom. Now we solemnly believe that
    it is the Lord's holy will that His child should feel the chastisement to be
    grievous; that the smartings of the rod should be felt. Moses, Jacob, Job,
    David, Paul, all were made to exclaim, &quot;The Lord has sorely chastened me.&quot;<br>
    <br>
    When it is remembered that our chastisements often grow out of our sin; that
    to subdue some strong indwelling corruption, or to correct for some outward
    departure, the rod is sent; this should ever humble the soul; this should
    ever cause the rebuke to be rightly viewed; that were it not for some strong
    indwelling corruption, or some step taken in departure from God, the
    affliction would have been withheld; oh how should every stroke of the rod
    lay the soul in the dust before God! &quot;If God had not seen sin in my heart,
    and sin in my outward conduct, He would not have dealt thus heavily with
    me.&quot; And where the grievousness of the chastisement is not felt, is there
    not reason to suspect that the cause of the chastisement has not been
    discovered and mourned over?<br>
    <br>
    There is the consideration, too, that the stroke comes from the Father who
    loves us; loves us so well, that if the chastisement were not needed, there
    would not be a feather's weight laid on the heart of his child. Dear to Him
    as the apple of His eye, would He inflict those strokes, if there were not
    an absolute necessity for them? &quot;What! Is it the Father who loves me that
    now afflicts me? Does this stroke come from His heart? What! Does my Father
    see all this necessity for this grievous chastening? Does He discover in me
    so much evil, so much perverseness, so much that He hates and that grieves
    Him, that this severe discipline is sent?&quot; Oh how does this thought, that
    the chastisement proceeds from the Father who loves him, impart a keenness
    to the stroke!<br>
    <br>
    And then there is often something in the very nature of the chastisement
    itself that causes its grievousness to be felt. The wound may be in the
    tenderest part; the rebuke may come through some idol of the heart; God may
    convert some of our choicest blessings into sources of the keenest sorrow.
    How often does He, in the wisdom and sovereignty of His dealings, adopt this
    method! Abraham's most valued blessing became the cause of his acutest
    sorrow. The chastisement may come through the beloved Isaac. The very mercy
    we clasp to our warm hearts so fondly may be God's voice to us, speaking in
    the tone of severe yet tender rebuke. Samuel, dear to the heart of Eli, was
    God's solemn voice to His erring yet beloved servant.<br>
    <br>
    Let no afflicted believer, then, think lightly of his chastisements– it is
    the Lord's will that he should feel them. They were sent for this purpose.
    If I did not feel the cross, if I was not conscious of the burden, if the
    wound were not painful, I should never take it to the mercy-seat, there to
    seek all needed grace, support, and strength. The burden must first be felt,
    before it is cast upon the Lord; the chastisement must be felt to be
    grievous, before the tenderness and sympathy of Jesus will be sought. <br>
    <br>
    There is equal danger of overrating our afflictions. When they are allowed
    too deeply to absorb us in grief; when they unfit us for duty; keep us from
    walking in the path God has marked out for us; hold us back from prayer and
    from the means of grace; when they lead us to think harshly and speak
    severely of God; then we overrate God's chastisements, and prevent the good
    they were so kindly sent to convey.<br>
    <br>
    <br>
