    OCTOBER 31.<br>
    <br>
    &quot;'Arise, shine; for your light is come, and the glory of the Lord is risen 
    upon you.&quot; Isaiah 40:1<br>
    <br>
    THERE are those whose lamps of Christian profession will not go out when the 
    Lord appears. They are His own chosen, redeemed, and called people. Their 
    light, by reason of manifold infirmities, may often have burned but dimly 
    through life; but there is vital religion in the soul-the golden precious 
    oil of grace, flowing from Jesus into their hearts; and this can never be 
    extinguished. Many were the hostile influences against which their weak 
    grace had to contend, many were the trials of their feeble faith, but the 
    light never quite went out. The waves of sorrow threatened to extinguish it; 
    the floods of inbred evil threatened to extinguish it; the cold blasts of 
    adversity threatened to extinguish it; and the stumbling of the walk, the 
    inconstancy of the heart, the declension of the soul, often for a while, 
    weakened and obscured it; but there it is, living, burning, and brightening, 
    as inextinguishable and as deathless as the source from where it came. The 
    grace of God in the heart is as imperishable, and the life of God in the 
    soul is as immortal, as God Himself. That light of knowledge enkindled in 
    the mind, and of love glowing in the heart, and of holiness shining in the 
    life, will burn in the upper temple in increasing effulgence of glory 
    through eternity. The divine light of Christian profession, which holy grief 
    for sin has enkindled, which love to God has enkindled, which the in-being 
    of the Holy Spirit has enkindled, will outshine and outlive the sun in the 
    firmament of heaven. That sun shall be extinguished, those stars shall fall, 
    and that moon shall be turned into blood, but the feeblest spark of grace in 
    the soul shall live forever. The Lord watches His own work with sleepless 
    vigilance. When the vessel is exhausted, He stands by and replenishes it; 
    when the light burns dimly, He is near to revive it; when the cold winds 
    blow rudely, and the rough waves swell high, He is riding upon those winds, 
    and walking upon those waves, to protect this the spark of His own kindling. 
    The light that is in you is light flowing from Jesus, the &quot;Fountain of 
    light.&quot; And can an infinite fountain be exhausted? When the sun is 
    extinguished, then all the lesser lights, deriving their faint effulgence 
    from Him, will be extinguished too-but not until then. Who is it that has 
    often fanned the smoking flax? Even He who will never quench the faintest 
    spark of living light in the soul. &quot;You will light my candle.&quot; And if the 
    Lord light it, what power can put it out? Is not His love the sunshine of 
    your soul? Is He not Himself your morning star? Is it not in His light that 
    you see light, even the &quot;light of the glory of God, in the face of Jesus 
    Christ&quot;? Oh, then, &quot;Arise and shine; for your light is come, and the glory 
    of the Lord is risen upon you.&quot;</p>
  </blockquote>
</blockquote>

<!--msnavigation--></td></tr><!--msnavigation--></table><!--msnavigation--><table border="0" cellpadding="0" cellspacing="0" width="100%"><tr><td>



</td></tr><!--msnavigation--></table></body>

</html>
