    <p align="justify">MAY 21. </p>
    <p align="justify">&quot;Is there no balm in Gilead? is there no physician 
    there?&quot; Jeremiah 8:22 </p>
    <p align="justify">There is! The physician is Jesus, the balm is His own 
    most precious blood. He binds up the broken heart, He heals the wounded 
    spirit. All the skill, all the efficacy, all the tenderness and crucial 
    sympathy needed for the office meet and center in Him in the highest degree. 
    Here then, disconsolate soul, bring your wounded heart. Bring it simply to 
    Jesus. One touch of His hand will heal the wound. One whisper of His voice 
    will hush the tempest. One drop of His blood will remove the guilt. Nothing 
    but a faith's application to Him will do for your soul now. Your case is 
    beyond the skill of all other physicians. Your wound is too deep for all 
    other remedies. It is a question of life and death, heaven or hell. It is an 
    emergency, a crisis, a turning point with you. Oh, how solemn, how eventful 
    is this moment! Eternity seems suspended upon it. All the intelligences of 
    the universe, good spirits and bad, seem gazing upon it with intense 
    interest. Decide the question, by closing in immediately with Jesus. Submit 
    to God. All things are ready. The blood is shed, the righteousness is 
    finished, the feast is prepared, God stands ready to pardon, yes, He 
    advances to meet you, His returning child, to fall upon your neck and 
    embrace you, with the assurance of His full and free forgiveness. <br>
    Let not the simplicity of the remedy keep you back. Many stumble at this. It 
    is but a look of faith: &quot;Look unto me, and be saved.&quot; It is but a touch, 
    even though with a palsied hand &quot;And as many as touched him were made 
    whole.&quot; It is but a believing the broad declaration, &quot;that Christ Jesus came 
    into the world to save sinners.&quot; You are not called to believe that He came 
    to save you; but that He saves sinners. Then if you inquire, &quot;But will He 
    save me? How do I know that if I come I shall meet a welcome?&quot; Our reply is, 
    only test Him. Settle not down with the conviction that you are too far 
    gone, too vile, too guilty, too unworthy, until you have gone and tried Him. 
    You know not how you wound Him, how you dishonor Him, and grieve the Spirit, 
    by yielding to a doubt, yes, the shadow of a doubt, as to the willingness 
    and the ability of Jesus to save you, until you have gone to Him 
    believingly, and put His readiness and His skill to the test. <br>
    Do not let the freeness of the remedy keep you away. This, too, is a 
    stumbling-block to many. Its very freeness holds them back. But it is 
    &quot;without money, and without price.&quot; The simple meaning of this is, no 
    worthiness on the part of the applicant, no merit of the creature, no tears, 
    no convictions, no faith, is the ground on which the healing is bestowed. Oh 
    no! It is all of grace- all of God's free gift, irrespective of any worth or 
    worthiness in man. Your strong motive to come to Christ is your very 
    sinfulness. The reason why you go to Him is that your heart is broken, and 
    that He only, can bind it up; your spirit is wounded, and that He only can 
    heal it; your conscience is burdened, and that He only can lighten it; your 
    soul is lost, and that He only, can save it. And that is all you need to 
    recommend you. It is enough for Christ that you are covered with guilt; that 
    you have no plea that springs from yourself; that you have no money to bring 
    in your hand, but have spent your all upon physicians, yet instead of 
    getting better you only grow worse; that you have wasted your substance in 
    riotous living, and now are insolvent; and that you really feel a drawing 
    towards Him, a longing for Him- that you ask, you seek, you crave, you 
    earnestly implore His compassion- that is enough for Him. His heart yearns, 
    His love is moved, His hand is stretched out- come and welcome to Jesus, 
    come. <br>
&nbsp; </p>
