    DECEMBER 26.<br>
    <br>
    &quot;For I am persuaded, that neither death, nor life, nor angels, nor 
    principalities, nor powers, nor things present, nor things to come, nor 
    height, nor depth, nor any other creature, shall be able to separate us from 
    the love of God, which is in Christ Jesus our Lord.&quot; Romans 8:38, 39<br>
    <br>
    THE love of the Father is seen in giving us Christ, in choosing us in 
    Christ, and in blessing us in Him with all spiritual blessings. Indeed, the 
    love of the Father is the fountain of all covenant and redemption mercy to 
    the Church. It is that river the streams whereof make glad the city of God. 
    How anxious was Jesus to vindicate the love of the Father from all the 
    suspicions and fears of His disciples! &quot;I say not unto you that I will pray 
    the Father for you; for the Father Himself loves you.&quot; &quot;God so loved the 
    world that He gave his only begotten Son.&quot; To this love we must trace all 
    the blessings which flow to us through the channel of the cross. It is the 
    love of God, exhibited, manifested, and seen in Christ Jesus; Christ being, 
    not the originator, but the gift of His love; not the cause, but the 
    exponent of it. Oh, to see a perfect equality in the Father&rsquo;s love with the 
    Son&rsquo;s love! Then shall we be led to trace all His present mercies, and all 
    His providential dealings, however trying, painful, and mysterious, to the 
    heart of God; thus resolving all into that from where all alike 
    flow--everlasting and unchangeable love.<br>
    <br>
    Now it is from this love there is no separation. &quot;Who shall separate us from 
    the love of Christ?&quot; The apostle had challenged accusation from every foe, 
    and condemnation from every quarter; but no accuser rose, and no 
    condemnation was pronounced. Standing on the broad basis of Christ&rsquo;s 
    finished work and of God&rsquo;s full justification, his head was now lifted up in 
    triumph above all his enemies round about. But it is possible that, though 
    in the believer&rsquo;s heart there is no fear of impeachment, there yet may exist 
    the latent one of separation. The aggregate dealings of God with His Church, 
    and His individual dealings with His saints, may at times present the 
    appearance of an alienated affection of a lessened sympathy. The age in 
    which this epistle was penned was fruitful of suffering to the Church of 
    God. And if any period or any circumstances of her history boded a severance 
    of the bond which bound her to Christ, that was the period, and those were 
    the circumstances. But with a confidence based upon the glorious truth on 
    which he had been descanting--the security of the Church of God in Christ--and 
    with a persuasion inspired by the closer realization of the glory about to 
    burst upon her view--with the most dauntless courage he exclaims, &quot;I am 
    persuaded that neither death, nor life, nor angels, nor principalities, nor 
    powers, nor things present, nor things to come, nor height, nor depth, nor 
    any other creature, shall be able to separate us from the love of God, which 
    is in Christ Jesus our Lord.&quot;<br>
    <br>
    <br>
