#!/bin/bash

MONTH=June
FILE_N=JUNE


csplit --elide-empty-files --prefix=$MONTH $FILE_N.htm  '/'$FILE_N'/' {*}
for file in $(find . -name "$MONTH*"); do cat -e $file | sed 's/M-^W/--/g' | sed 's/M-^S/\&quot;/g' | sed 's/M-^T/\&quot;/g' | sed 's/M-^R/\&rsquo;/g' > readings/$file; done
