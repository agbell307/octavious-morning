#!/bin/bash

##################
#
# Create a feed of Octavius Winston's Daily Morning Readings
#
##################

##################
#
# Update Podcast
#
##################

M_DAY=$(date +"%B%d")
M_DATE=$(date +"%Y%m%d")
CONTENT=$(cat /var/www/html/grieg.bellclan/octavious/morning-readings/$M_DAY)

cat > /tmp/ODR-morning.txt <<- _EOF_
        <item>
                <title>Morning Daily Readings by Octavious Winslow</title>
                <link> http://grieg.bellclan.xyz/octavious/morning-readings/$M_DAY </link>
                <guid> ODRM_$M_DATE</guid>
                <description><![CDATA[
			 $CONTENT 
			]]>
		</description>
        </item>
_EOF_

#sed -i '44,+6d' /home/aaron/AIO/KVIP/FeedKVIP.xml
#sed -i '8r /tmp/ODR-morning.txt' FeedODRM.xml
sed -i '8r /tmp/ODR-morning.txt' /home/aaron/html/bach/FeedODRM.xml

